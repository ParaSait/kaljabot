(ns kaljabot.turdle
  (:require [clojure.string :as str]
            [clojure.walk :as w]
            [kaljabot.util :as u]
            [kaljabot.markdown :as md]
            [kaljabot.emoji :as e]
            [kaljabot.constants :as c]
            [kaljabot.turdle-dictionary :as td]
            [kaljabot.turdle-score :as ts]
            [kaljabot.dictionary-api :as dict]))

;; TODO letterle mode
;; TODO reveal first letter in wrdl mode
;; TODO anti-sniping mode
;; TODO nerdle mode
;; TODO render the board as an image
;; TODO quordle mode
;; TODO codebreaker mode
;; TODO add more languages

(defn load-mode-data!
  []
  (->> (u/read-json-from-file! c/turdle-modes-path)
       (map (fn [[mode mode-attrs]] {mode (w/keywordize-keys mode-attrs)}))
       (into {})))

(defn pickable-word?
  [length word]
  (and (= (count word) length)
       (not-any? (complement u/is-letter?) word)))

(defn game-accepts-as-guess?
  [{word :word} input]
  (pickable-word? (count word) input))

(defn pick-word ;; TODO handle when it's nil
  [length]
  (->> (td/load-dictionary!)
       (filter (partial pickable-word? length))
       (rand-nth)))

(defn create-game
  [mode player]
  (if-let [mode-attrs (get (load-mode-data!) mode)]
    {:mode mode
     :mode-attrs mode-attrs
     :player player
     :word (pick-word (get mode-attrs :word-length))
     :guesses []
     :max-guesses (get mode-attrs :max-guesses)}
    (throw (ex-info "Nonexistent game mode" {:type :nonexistent-game-mode :mode mode}))))

(defn decide-score
  [{word        :word
    guesses     :guesses
    max-guesses :max-guesses}]
  (cond (some (partial = word) guesses) (count guesses)
        (= (count guesses) max-guesses) 0
        :else                           nil))

(def status-absent  0)
(def status-present 1)
(def status-correct 2)

(defn make-letter-counts
  [word]
  (reduce #(update %1 %2 (fnil inc 0)) {} word))

(defn- do-breakdown-pass
  [word guess status-f {counts :counts statuses :statuses}]
  (loop [word*        word
         guess*       guess
         counts*      counts
         old-statuses statuses
         new-statuses []]
    (if (empty? word*)
      {:counts   counts*
       :statuses new-statuses}
      (let [new-status (when-not (first old-statuses)
                         (status-f (first word*) (first guess*)
                                   (> (or (get counts* (first guess*)) 0) 0)))
            next-status (if new-status
                          new-status
                          (first old-statuses))]
        (recur (rest word*)
               (rest guess*)
               (if new-status
                 (update counts* (first guess*) (fnil dec 0))
                 counts*)
               (rest old-statuses)
               (conj new-statuses next-status))))))

(defn make-breakdown
  [word guess]
  (let [pass (partial do-breakdown-pass word guess)]
    (->> {:counts   (make-letter-counts word)
          :statuses []}
         (pass (fn [word-letter guess-letter any-left]
                 (when (and any-left (= word-letter guess-letter))
                   status-correct)))
         (pass (fn [word-letter guess-letter any-left]
                 (when (and any-left (str/includes? word (str guess-letter)))
                   status-present)))
         (pass (fn [_ _ _]
                 status-absent))
         (:statuses)
         (map (fn [& args] args)
              guess)
         (map-indexed (fn [index [letter status]]
                        [index letter status])))))

(defn gather-knowledge-from-game
  [{word    :word
    guesses :guesses}]
  (->> guesses
       (mapcat (partial make-breakdown word))
       (distinct)))

(defn make-letter-report-from-knowledge
  [knowledge]
  (->> knowledge
       (map (fn [[index letter status]] (hash-map letter status)))
       (apply merge-with max)))

(defn make-letter-report-for-game
  [game]
  (->> game
       (gather-knowledge-from-game)
       (make-letter-report-from-knowledge)))

(defn- format-broke-down-letter
  [[index letter status]]
  (cond
    (= status status-absent)  (md/strikethrough (md/monospace (str "[" (str/upper-case (str letter)) "]")))
    (= status status-present) (md/bold          (md/monospace (str "[" (str/upper-case (str letter)) "]")))
    (= status status-correct) (md/bold          (md/monospace (str " " (str/upper-case (str letter)) " ")))))

(defn- render-board
  [{word        :word
    guesses     :guesses
    max-guesses :max-guesses}]
  (concat (map (comp (partial str/join " ")
                     (partial map format-broke-down-letter)
                     (partial make-breakdown word))
               guesses)
          (repeat (- max-guesses (count guesses))
                  (str/join " " (repeat (count word) (md/monospace "[-]"))))))

(def alphabet (map char (range (int \a) (inc (int \z)))))

(defn- render-remaining-letters
  [breakdown-aggregation]
  (->> alphabet
       (eduction (comp (map    (fn [letter]     [letter (get breakdown-aggregation letter)]))
                       (remove (fn [[_ status]] (= status status-absent)))
                       (map    (fn [[letter status]]
                                 (cond
                                   (= status nil)            (str/upper-case (str letter))
                                   (= status status-present) (md/underline (str/upper-case (str letter))) 
                                   (= status status-correct) (md/bold      (str/upper-case (str letter))))))))
       (str/join " ")))

(defn render-game
  [game]
  (->> (concat (render-board game)
               [""]
               [(render-remaining-letters (make-letter-report-for-game game))])
       (str/join "\n")
       (md/multiquote)))

(defn- pick-intro-message
  [mode-attrs]
  (rand-nth ["Hope you don't get a bullshit word!"
             "Hopefully no bullshit word this time!"
             "May the force of shitting be with you!"
             "Here we go for today's letters!"
             "Go for it, dude!"
             "We accept 'strol' here!"
             "Time to drop a big, smelly turd of a word!"
             (str "The only thing that isn't shit about this is the fact that it runs on " (or c/os-name "linux") "!")
             "I like short words and I cannot lie!"
             "Now with 20% more bullshit!"
             "These shit words are of grade A quality!"
             "Try not to fuck up your streak!"
             "The perfect game for when you're taking a shit on the toilet!"
             "Fresh from the bull's ass!"
             "Our new formula: 40% bullshit. 35% horseshit. 20% pig shit. 5% chicken shit."
             "Here comes a big ol' steamer!"
             (str "I'm sure this will be such " (md/strikethrough "bullshit") " fun!")]))

(def ^{:private true} motd
  (str (md/bold "Did you know:") " you can now set your personal default game mode.\n"
       "Enter " (md/monospace "!pref set turdle-mode <mode>") " to set your default game mode.\n"
       "Enter " (md/monospace "!pref get turdle-mode") " to see what the current default is set to.\n"
       "And remember, enter " (md/monospace "!turdle modes") " for an overview of available modes."))

(defn start-game
  [message-f mode player]
  (let [game (create-game mode player)]
    (message-f (str "Welcome to turdle! You are playing " (md/monospace mode) ". "
                    (pick-intro-message (get game :mode-attrs)) "\n"
                    motd))
    (message-f (render-game game))
    game))

(defn- msg-lines
  [& parts]
  (->> (apply concat parts)
       (clojure.string/join "\n")))

(defn render-user-score-section
  ([stats score]
   (msg-lines (map-indexed (fn [idx total]
                             (->> (cond (= idx 0) (str "dead - " (md/bold total))
                                        (= idx 1) (str idx " guess - " (md/bold total))
                                        :else     (str idx " guesses - " (md/bold total)))
                                  ((if (= idx score) md/underline identity))))
                           (get stats "scores"))
              (case score ;; TODO make congrats messages part of the game mode
                3 [(str e/dd " " (md/bold (md/italic "Amazing")) " " e/dd)]
                2 [(str e/menri " " (md/bold (md/italic "Based")) " " e/menri)]
                1 [(str e/dovahkiin " " (md/bold (md/italic "DOVAHKIIN")) " " e/dovahkiin)]
                [])))
  ([stats]
   (render-user-score-section stats nil)))

(defn render-user-streak-section
  [stats [overall-best-streak overall-best-players] username-f]
  (let [streak               (get stats "streak")
        personal-best-streak (get stats "best-streak")]
    (msg-lines [(str "Current streak - "       (md/bold streak))]
               [(str "Personal best streak - " (md/bold personal-best-streak))]
               [(str "Overall best streak - "  (md/bold overall-best-streak) " (" (clojure.string/join ", " (map username-f overall-best-players)) ")")]
               (cond (= streak overall-best-streak)  [(str e/heyhey " " (md/bold (md/italic "Overall best streak!")) " " e/heyhey)]
                     (= streak personal-best-streak) [(str e/clap " " (md/bold (md/italic "Personal best streak!")) " " e/clap)]
                     :else                           []))))

(defn render-user-scores
  [mode user username-f]
  (let [score-data (ts/load-score-data!)]
    (if-let [stats (ts/find-stats-for-user score-data mode user)]
      (msg-lines [(str "Scores for " (username-f user) ":")]
                 [(render-user-score-section stats)])
      (str (username-f user) " hasn't played yet!"))))

(defn render-streak-overview
  [mode username-f]
  (let [score-data      (ts/load-score-data!)
        streak-overview (ts/query-streak-overview-info score-data mode)]
    (if-not (empty? streak-overview)
      (let [highest-streak      (->> streak-overview
                                     (map (fn [[_ {streak :streak}]] streak))
                                     (apply max))
            highest-best-streak (->> streak-overview
                                     (map (fn [[_ {best-streak :best-streak}]] best-streak))
                                     (apply max))]
        (msg-lines ["Streak overview:"]
                   (mapv (fn [[user {streak      :streak
                                     best-streak :best-streak}]]
                           (str (username-f user) " - "
                                "Current: " ((if (= streak highest-streak) md/bold identity) streak) ", "
                                "Best: " ((if (= best-streak highest-best-streak) md/bold identity) best-streak) "."))
                         streak-overview)))
      "No one has played it yet!")))

(defn render-modes-overview
  []
  (let [times-played-per-mode (ts/query-times-played-per-mode (ts/load-score-data!))]
    (msg-lines ["Available game modes:"]
               (->> (load-mode-data!)
                    (sort-by (fn [[mode _]]
                               [(- (or (get times-played-per-mode mode) 0))
                                mode]))
                    (map (fn [[mode {description :description}]]
                           (let [times-played (or (get times-played-per-mode mode) 0)]
                             (str (md/bold mode) " ("
                                  (cond (= times-played 0) "not played yet"
                                        (= times-played 1) (str "played " times-played " time")
                                        :else              (str "played " times-played " times"))
                                  ") - " description))))))))

(defn render-user-stats
  [score-data mode user score username-f]
  (let [stats (ts/find-stats-for-user score-data mode user)
        overall-best-streak-info (ts/query-overall-best-streak-info score-data mode)]
    (msg-lines [(md/bold (md/underline "Scores"))]
               [(render-user-score-section stats score)]
               [""]
               [(md/bold (md/underline "Streaks"))]
               [(render-user-streak-section stats overall-best-streak-info username-f)])))

(defn valid-word?!
  [word]
  (if (td/contains-word? word)
    true
    (if (dict/word-exists?! word)
      (do (td/add-word! word)
          true)
      false)))

(defn guess-word
  [message-f game word]
  (if (valid-word?! word)
    (update game :guesses #(conj % word))
    (do (message-f (str (md/monospace word) " is either not a valid word, or the dictionary server is just having trouble finding it. "
                        "Unfortunately, the dictionary server I'm currently using can't seem to tell the difference.")) ;; TODO do something about this problem
        nil)))

(defn update-game ;; TODO test
  [message-f username-f game word]
  (try
    (if-let [game* (guess-word message-f game word)]
      (if-let [score (decide-score game*)]
        (let [score-data (ts/tally-score! (get game :mode)
                                          (get game :mode-attrs)
                                          (get-in game [:player :user])
                                          score)]
          (do (message-f (render-game game*))
              (message-f (str (if (= score 0)
                                (str "You lose! " (rand-nth [e/musk e/schoppy e/bullshit]) " The word was " (md/monospace (:word game)) ".\n")
                                (str "You win! " (rand-nth [e/elon e/aratilt e/mike]) "\n"))
                              "\n"
                              (render-user-stats score-data
                                                 (get game :mode)
                                                 (get-in game [:player :user])
                                                 score
                                                 username-f)))
              nil))
        (do (message-f (render-game game*))
            game*))
      game)
    (catch clojure.lang.ExceptionInfo ex ;; presumably this comes from dictionaryapi
      (do (message-f (str "I had some trouble determining if " (md/monospace word) " is valid or not. Try it again in a bit maybe."))
          game))))
