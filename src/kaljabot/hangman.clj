(ns kaljabot.hangman
  (:require [clojure.string :as str]
            [kaljabot.hangman-score :as hs]
            [kaljabot.markdown :as md]
            [kaljabot.util :as u]))

(def stages
  [(str "     \n"
        "     \n"
        "     \n"
        "     \n"
        "____ \n")
   (str "     \n"
        "    |\n"
        "    |\n"
        "    |\n"
        "____|\n")
   (str "  --+\n"
        "    |\n"
        "    |\n"
        "    |\n"
        "____|\n")
   (str " ,--+\n"
        "    |\n"
        "    |\n"
        "    |\n"
        "____|\n")
   (str " ,--+\n"
        " O  |\n"
        "    |\n"
        "    |\n"
        "____|\n")
   (str " ,--+\n"
        " O  |\n"
        " |  |\n"
        "    |\n"
        "____|\n")
   (str " ,--+\n"
        "_O  |\n"
        " |  |\n"
        "    |\n"
        "____|\n")
   (str " ,--+\n"
        "_O_ |\n"
        " |  |\n"
        "    |\n"
        "____|\n")
   (str " ,--+\n"
        "_O_ |\n"
        " |  |\n"
        "/   |\n"
        "____|\n")
   (str " ,--+\n"
        "_O_ |\n"
        " |  |\n"
        "/ \\ |\n"
        "____|\n")])

(def words (remove (fn [word] (< (count word) 5))
                   (str/split-lines (slurp "resources/hangman_words.txt"))))

(defn pick-word
  []
  (str/lower-case (rand-nth words)))

(defn letter-guessed?
  [game letter]
  (some #{letter} (:guesses game)))

(defn good-guess?
  [game letter]
  (str/index-of (:word game) letter))

(defn revealed?
  [game letter]
  (or (not (u/is-letter? letter))
      (some #{letter} (:guesses game))))

(defn all-letters-guessed?
  [game]
  (every? (partial revealed? game) (:word game)))

(defn add-mistake
  [game]
  (update game :mistakes inc))

(defn guess-letter
  [game letter]
  (update game :guesses #(conj % letter)))

(defn is-dead?
  [game]
  (>= (:mistakes game) (count stages)))

(defn is-word?
  [game word]
  (= word (:word game)))

(defn calc-rank
  [game]
  (let [word-length-intermediate (- 10 (count (:word game)))
        word-length-score (if (< word-length-intermediate 0) 0 word-length-intermediate)
        vowels-used (count (clojure.set/intersection #{\a \e \i \o \u} (:guesses game)))
        vowel-score (case vowels-used
                      0 10
                      1 5
                      2 3
                      3 2
                      4 1
                      0)
        mistake-score (case (:mistakes game)
                        0 20
                        1 10
                        2 8
                        3 6
                        4 5
                        5 4
                        6 3
                        7 2
                        8 1
                        0)
        total-score (+ word-length-score
                       vowel-score
                       mistake-score)]
    (cond (>= total-score 35) "DOVAHKIIN"
          (>= total-score 30) "S+"
          (>= total-score 20) "S"
          (>= total-score 15) "A"
          (>= total-score 10) "B"
          (>= total-score 7) "C"
          (>= total-score 5) "D"
          (>= total-score 2) "E"
          :else "F")))

(defn render-guessed-word
  [game]
  (apply str (map (fn [letter]
                    (if (revealed? game letter)
                      letter
                      "."))
                  (:word game))))

(defn render-guesses
  [game]
  (str "guessed: " (str/join ", " (sort (:guesses game))) "."))

(defn create-game
  [player]
  {:player   player
   :word     (pick-word)
   :guesses  #{}
   :mistakes 0})

(defn start-game
  [message-f player]
  (let [game (create-game player)]
    (message-f (md/monospace (render-guessed-word game)))
    game))

(defn mistake-routine
  [message-f game]
  (let [game* (add-mistake game)
        stage-str (str (md/codeblock (stages (dec (:mistakes game*)))) "\n")
        rendered-word-str (str (md/monospace (render-guessed-word game*)) "\n\n")]
    (if (is-dead? game*)
      (let [user (get-in game [:player :user])
            scores (hs/tally-score! user "dead")]
        (message-f (str stage-str "you're dead! the word was: " (md/monospace (:word game*))
                        "\n\n" "your stats: " (md/monospace (hs/format-user-scores-for-message scores user))))
        nil)
      (do
        (message-f (str stage-str rendered-word-str (render-guesses game*)))
        game*))))

(defn- win-routine
  [message-f game msg]
  (let [user (get-in game [:player :user])
        rank   (calc-rank game)
        scores (hs/tally-score! user rank)]
    (message-f (str msg rank "\n\n" "your stats: " (md/monospace (hs/format-user-scores-for-message scores user))))
    nil))

(defn update-game-guess-letter
  [message-f game letter]
  (if-not (letter-guessed? game letter)
    (let [game* (guess-letter game letter)]
      (if (good-guess? game letter)
        (do
          (let [rendered-word-str (str (md/monospace (render-guessed-word game*)) "\n\n")]
            (if (all-letters-guessed? game*)
              (win-routine message-f game* (str rendered-word-str "you win! rank: "))
              (do
                (message-f (str rendered-word-str (render-guesses game*)))
                game*))))
        (mistake-routine message-f game*)))
    (do
      (message-f "you already tried that one!")
      game)))

(defn update-game-guess-word
  [message-f game word]
  (if (is-word? game word)
    (win-routine message-f game "correct! you win! rank: ")
    (mistake-routine message-f game)))