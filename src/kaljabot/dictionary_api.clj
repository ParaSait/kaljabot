(ns kaljabot.dictionary-api
  (:require [clj-http.client :as http]
            [clojure.data.json :as json])
  (:use [slingshot.slingshot :only [try+]]))

(defn word-exists?!
  [word]
  (try+
    (let [status (:status (http/head (str "https://api.dictionaryapi.dev/api/v2/entries/en/" word)))]
      (= status 200))
    (catch [:status 404] {:keys [request-time headers body]}
      false)
    (catch Object _
      (throw (ex-info "Unexpected response from dictionaryapi." {})))))