(ns kaljabot.command-management
  (:require [kaljabot.constants :as c]
            [kaljabot.commands :as cmd]
            [kaljabot.discord-api :as da]
            [clojure.string :as str]
            [clojure.walk :as w])
  (:import (java.io BufferedReader
                    InputStreamReader))
  (:gen-class))

(defn command-diff
  [target-cmds current-cmds]
  (concat (->> target-cmds
               (remove (fn [target-cmd]
                         (some (fn [current-cmd]
                                 (= (get target-cmd :name)
                                    (get current-cmd :name)))
                               current-cmds)))
               (map (fn [cmd]
                      [:create cmd])))
          (->> target-cmds
               (map (fn [target-cmd]
                      (if-let [existing-cmd (some (fn [current-cmd]
                                                    (when (= (get target-cmd :name)
                                                             (get current-cmd :name))
                                                      current-cmd))
                                                  current-cmds)]
                        (when-not (= target-cmd
                                     (select-keys existing-cmd (keys target-cmd)))
                          (assoc target-cmd :id (get existing-cmd :id))))))
               (remove nil?)
               (map (fn [cmd]
                      [:update cmd])))
          (->> current-cmds
               (remove (fn [current-cmd]
                         (some (fn [target-cmd]
                                 (= (get current-cmd :name)
                                    (get target-cmd :name)))
                               target-cmds)))
               (map (fn [cmd]
                      [:delete cmd])))))

(defmulti transact-diff-item!
  (fn [_ d] (first d)))

(defmethod transact-diff-item! :create
  [app d]
  (da/create-global-command! app
                             (w/stringify-keys (second d))))

(defmethod transact-diff-item! :update
  [app d]
  (da/edit-global-command! app
                           (get (second d) :id)
                           (w/stringify-keys (dissoc (second d) :id))))

(defmethod transact-diff-item! :delete
  [app d]
  (da/delete-global-command! app
                             (get (second d) :id)))

(defn transact-diff!
  [app cmd-diff]
  (dorun (map (partial transact-diff-item! app) cmd-diff)))

(defn- get-current-commands!
  []
  (-> (da/get-global-commands! c/application-id)
      (w/keywordize-keys)))

(defn- prompt!
  [question]
  (print question)
  (flush)
  (read-line))

(defn- request-transaction-approval!
  []
  (= (str/lower-case (prompt! "Proceed with transaction? (y/N) ")) "y"))

(defn -main
  [& args]
  (println "Comparing current commands with target commands...")
  (let [d (command-diff cmd/all-cmds (get-current-commands!))]
    (if (empty? d)
      (do
        (println "No changes are needed.")
        (System/exit 0))
      (do
        (println "The following changes are needed:")
        (clojure.pprint/pprint d)
        (if (request-transaction-approval!)
          (do
            (println "Transacting changes...")
            (transact-diff! c/application-id d)
            (println "Transaction completed.")
            (System/exit 0))
          (do
            (println "Transaction cancelled.")
            (System/exit 1)))))))