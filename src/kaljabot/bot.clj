(ns kaljabot.bot
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [kaljabot.discord-api :as da]
            [kaljabot.emoji :as e]
            [kaljabot.constants :as c]
            [kaljabot.bullshit :as bs]
            [kaljabot.hangman :as hm]
            [kaljabot.turdle :as trd]
            [kaljabot.user-prefs :as pref]
            [kaljabot.util :as u]
            [kaljabot.markdown :as md]
            [kaljabot.interaction :as i]))

(defmulti handle-event!
          (fn [type _ _] type))

(defmethod handle-event! :default
  [type _ _]
  (println (str "unknown event type: " type)))

(defmethod handle-event! "READY"
  [_ evt {session-id :session-id
          own-user :own-user}]
  (reset! session-id (get evt "session_id"))
  (reset! own-user (get-in evt ["user" "id"])))

(defn mentioned?
  [evt user]
  (or (get evt "mention_everyone")
      (some (fn [u] (= user (get u "id")))
            (get evt "mentions"))))

(defn interject
  [channel evt user chance message]
  (when (or (< (rand) chance)
            (mentioned? evt user))
    (da/create-message! channel message)))

(def aux-verbs ["is" "isn't" "was" "wasn't"
                "are" "aren't" "were" "weren't"
                "does" "doesn't" "did" "didn't"
                "do" "don't"
                "has" "hasn't" "had" "hadn't"
                "have" "haven't"
                "will" "won't" "would" "wouldn't"
                "can" "can't" "could" "couldn't"
                "shall" "shan't" "should" "shouldn't"])

(def bad-aux-verbs ["isnt" "wasnt"
                    "arent" "werent"
                    "doesnt" "didnt"
                    "dont"
                    "hasnt" "hadnt"
                    "havent"
                    "wont" "wouldnt"
                    "cant" "couldnt"
                    "shant" "shouldnt"])

(defn yes-no-question?
  [msg]
  (if-let [idx (str/index-of msg \space)]
    (.contains aux-verbs (subs msg 0 idx))))

(defn bad-aux-verb?
  [msg]
  (if-let [idx (str/index-of msg \space)]
    (.contains bad-aux-verbs (subs msg 0 idx))))

(defn looks-like-number?
  [msg]
  (or (re-matches #"^\d+$" msg)
      (and (re-matches #".*\d+.*" msg)
           (re-matches #".*tomorrow.*" msg))))

(defn roll-dice
  [dice sides]
  (if (> dice 0)
    (+ (inc (rand-int sides))
       (roll-dice (dec dice) sides))
    0))

(def mention-pattern #"\<@!?\d+\>")

(defn strip-mentions
  [msg]
  (-> msg
      (str/replace mention-pattern "")
      (str/replace #" +" " ")
      (str/trim)))

(defmulti handle-command!
  (fn [cmd _ _ _ _ _ _] cmd))

(defmulti handle-interaction!
  (fn [interaction state]
    (-> interaction
        (i/data)
        (i/command-name))))

(defmethod handle-command! :default
  [cmd _ _ _ channel _ _]
  (da/create-message! channel (str "unrecognized command: " cmd)))

(defn respond-to-version-command
  [channel]
  (when (.exists (io/file c/version-file-path))
    (let [version-data (slurp c/version-file-path)
          version-string (str/replace version-data #"\r?\n" "")] ;; there may be a line feed at the end of the file; remove it
      (str "My current version is " version-string "."))))

(defmethod handle-command! "version"
  [_ _ _ _ channel _ _]
  (->> (respond-to-version-command channel)
       (da/create-message! channel)))

(defmethod handle-interaction! "version"
  [interaction state]
  (-> (respond-to-version-command (i/channel interaction))
      (i/->message-response)
      (i/respond interaction)))

(defn respond-to-roll-command
  [args channel]
  (let [arg (or (first args) "")
        roll (cond
               (re-matches #"\d+" arg)
               (roll-dice 1 (Integer/parseInt arg))
               (re-matches #"\d+d\d+" arg)
               (let [parts (str/split arg #"d")]
                 (roll-dice (Integer/parseInt (first parts))
                            (Integer/parseInt (second parts))))
               :else
               (roll-dice 1 100))]
    (str roll)))

(defmethod handle-command! "roll"
  [_ args _ _ channel _ _]
  (->> (respond-to-roll-command args channel)
       (da/create-message! channel)))

(defmethod handle-interaction! "roll"
  [interaction state]
  (-> (respond-to-roll-command [(get (i/command-opts (i/data interaction)) :dice)]
                               (i/channel interaction))
      (i/->message-response)
      (i/respond interaction)))

(defn respond-to-bullshit-command
  [args channel]
  (let [full-str (nth args 0)
        empty-str (nth args 1)
        string (str/join " " (subvec (vec args) 2))
        bullshit (bs/generate-bullshit string full-str empty-str)]
    (if (< (count bullshit) c/discord-char-limit)
      bullshit
      "that would be too large a message for now, para will have to program it so that it gets like split up and stuff lol")))

(defmethod handle-command! "bullshit"
  [_ args _ _ channel _ _]
  (->> (respond-to-bullshit-command args channel)
       (da/create-message! channel)))

(defmethod handle-interaction! "bullshit"
  [interaction state]
  (let [opts (i/command-opts (i/data interaction))]
    (-> (respond-to-bullshit-command [(get opts :fg)
                                      (get opts :bg)
                                      (get opts :text)]
                                     (i/channel interaction))
        (i/->message-response)
        (i/respond interaction))))

(defn react-to-hangman-command
  [channel user state]
  (swap! (:hangman-game state)
         (fn [hangman-game]
           (if-not hangman-game
             (hm/start-game (partial da/create-message! channel)
                            {:channel channel :user user})
             hangman-game))))

(defmethod handle-command! "hangman"
  [_ _ _ _ channel user state]
  (react-to-hangman-command channel user state))

(defmethod handle-interaction! "hangman"
  [interaction state]
  (i/respond (i/->message-response "Welcome to Hangman!") interaction)
  (react-to-hangman-command (i/channel interaction)
                            (i/user-id interaction)
                            state))

(defn parse-turdle-args
  [args]
  (loop [state :initial
         args* args]
    (let [next   (first args*)
          args** (rest args*)]
      (letfn [(error []
                (throw (ex-info "Invalid arguments to the turdle command." {:args args})))
              (final [o]
                (if (empty? args*)
                  o
                  (error)))]
        (case state
          :initial
          (case next
            nil       (final {:action :play})
            "play"    (recur :play args**)
            "scores"  (recur :scores args**)
            "streaks" (recur :streaks args**)
            "modes"   (recur :modes args**)
            (recur :implied-play-with-mode args**))

          :play
          (if next
            (recur :play-with-mode args**)
            (final {:action :play}))
          
          :play-with-mode
          (final {:action :play
                  :mode (nth args 1)})
          
          :scores
          (if next
            (recur :scores-with-mode args**)
            (final {:action :scores}))

          :scores-with-mode
          (if next
            (recur :scores-with-mode-and-player-name args**)
            (final {:action :scores
                    :mode (nth args 1)}))

          :scores-with-mode-and-player-name
          (final {:action :scores
                  :mode (nth args 1)
                  :player-name (nth args 2)})
          
          :streaks
          (if next
            (recur :streaks-with-mode args**)
            (final {:action :streaks}))
          
          :streaks-with-mode
          (final {:action :streaks
                  :mode (nth args 1)})
          
          :implied-play-with-mode
          (final {:action :play
                  :mode (nth args 0)})

          :modes
          (final {:action :modes}))))))

(defn turdle-slashargs->oldargs ;; TODO dirty dirty hack, get rid of it...
  [slashargs]
  (let [command (name (first (first slashargs)))
        options (second (first slashargs))]
    (if (empty? options)
      [command]
      (if (= command "scores")
        (if (contains? options :player)
          [command (get options :mode) (da/get-user! (get options :player))]
          [command (get options :mode)])
        (let [option-key (first (first options))
              option-val (second (first options))]
          (if (= :player option-key)
            [command (da/get-user! option-val)]
            [command option-val]))))))

(defn react-to-turdle-command
  [args guild channel user state]
  (let [{action      :action
         mode        :mode
         player-name :player-name}
        (try
          (parse-turdle-args args)
          (catch clojure.lang.ExceptionInfo ex
            {:action :error}))
        message-f  (partial da/create-message! channel)
        username-f (comp #(get % "username") da/get-user!)
        mode*      (or mode (pref/get-user-pref! user "turdle-mode"))]
    (case action
      :play
      (try
        (swap! (:turdle-game state)
               (fn [turdle-game]
                 (if-not turdle-game
                   (trd/start-game message-f mode* {:channel channel :user user})
                   turdle-game)))
        (catch clojure.lang.ExceptionInfo ex
          (if (= (:type (ex-data ex)) :nonexistent-game-mode)
            (message-f (str "Mode \"" (:mode (ex-data ex)) "\" doesn't exist. Enter " (md/monospace "!turdle modes") " for the list of available modes."))
            (throw ex))))
      :scores
      (if-let [player-user (if player-name
                             (da/find-guild-user-by-name! guild player-name)
                             user)]
        (message-f (trd/render-user-scores mode* player-user username-f))
        (message-f (str player-name "? Who's that?")))
      :streaks
      (message-f (trd/render-streak-overview mode* username-f))
      :modes
      (message-f (trd/render-modes-overview))
      :error
      (message-f (str "Invalid parameters.")))))

(defmethod handle-command! "turdle" ;; TODO don't allow hangman games while a turdle game is in progress, and vice versa
  [_ args _ guild channel user state]
  (react-to-turdle-command args guild channel user state))

(defmethod handle-interaction! "turdle"
  [interaction state]
  (i/respond (i/->message-response "Welcome to Turdle!") interaction)
  (react-to-turdle-command (turdle-slashargs->oldargs (i/command-opts (i/data interaction)))
                           (i/guild interaction)
                           (i/channel interaction)
                           (i/user-id interaction)
                           state))

(defn handle-turdle-guess!
  [args channel user state]
  (let [guess (str/lower-case (str/join " " args))
        message-f (partial da/create-message! channel)
        username-f (comp #(get % "username") da/get-user!)]
    (swap! (:turdle-game state)
           (fn [turdle-game]
             (if (and turdle-game
                      (not (str/blank? guess))
                      (= {:channel channel :user user} (:player turdle-game)))
               (trd/update-game message-f username-f turdle-game guess)
               turdle-game)))))

(defn react-to-guess-command
  [args channel user state]
  (let [guess (str/lower-case (str/join " " args))
        message-f (partial da/create-message! channel)]
    (swap! (:hangman-game state)
           (fn [hangman-game]
             (if (and hangman-game
                      (not (str/blank? guess))
                      (= {:channel channel :user user} (:player hangman-game)))
               (if (and (= (count guess) 1) (u/is-letter? (first guess)))
                 (hm/update-game-guess-letter message-f hangman-game (first guess))
                 (hm/update-game-guess-word message-f hangman-game guess))
               hangman-game)))))

(defmethod handle-command! "guess" ;; TODO don't allow hangman games while a turdle game is in progress, and vice versa
  [_ args _ _ channel user state]
  (react-to-guess-command args channel user state))

(defmethod handle-interaction! "guess"
  [interaction state]
  (i/respond (i/->message-response "Let's see if that's right!") interaction)
  (react-to-guess-command [(get (i/command-opts (i/data interaction)) :word)]
                          (i/channel interaction)
                          (i/user-id interaction)
                          state))

(defn parse-pref-args
  [args]
  (loop [state :initial
         args* args]
    (let [next   (first args*)
          args** (rest args*)]
      (letfn [(error []
                (throw (ex-info "Invalid arguments to the pref command." {:args args})))
              (final [o]
                (if (empty? args*)
                  o
                  (error)))]
        (case state
          :initial
          (case next
            nil   (error)
            "set" (recur :set args**)
            "get" (recur :get args**)
            (error))
          
          :set
          (if next
            (recur :set-key args**)
            (error))
          
          :set-key
          (if next
            (recur :set-key-value args**)
            (error))

          :set-key-value
          (final {:action :set
                  :key (nth args 1)
                  :value (nth args 2)})
          
          :get
          (if next
            (recur :get-key args**)
            (error))
          
          :get-key
          (final {:action :get
                  :key (nth args 1)}))))))

(defn pref-slashargs->oldargs
  [slashargs]
  (let [command (name (first (first slashargs)))
        options (second (first slashargs))]
    (cond
      (= command "set") [command (get options :key) (get options :value)]
      (= command "get") [command (get options :key)])))

(defn respond-to-pref-command
  [args user]
  (let [{action :action
         key    :key
         value  :value}
        (try
          (parse-pref-args args)
          (catch clojure.lang.ExceptionInfo ex
            {:action :error}))]
    (case action
      :set
      (try
        (pref/set-user-pref! user key value)
        (str "Preference " (md/monospace key) " has been set to " (md/monospace value) ".")
        (catch clojure.lang.ExceptionInfo ex
          (case (:type (ex-data ex))
            :invalid-user-pref-key   (str "Invalid preference: " (md/monospace key))
            :invalid-user-pref-value (str "Invalid value for preference " (md/monospace key) ": " (md/monospace value)))))
      :get
      (try
        (str "Preference " (md/monospace key) " is currently set to " (md/monospace (pref/get-user-pref! user key)) ".")
        (catch clojure.lang.ExceptionInfo ex
          (case (:type (ex-data ex))
            :invalid-user-pref-key   (str "Invalid preference: " (md/monospace key)))))
      :error
      "Invalid parameters.")))

(defmethod handle-command! "pref"
  [_ args _ _ channel user _]
  (->> (respond-to-pref-command args user)
       (da/create-message! channel)))

(defmethod handle-interaction! "pref"
  [interaction state]
  (-> (respond-to-pref-command (pref-slashargs->oldargs (i/command-opts (i/data interaction)))
                               (i/user-id interaction))
      (i/->message-response)
      (i/respond interaction)))

;; TODO post random message from the channel when mentioned
;; TODO youtube searching
(defmethod handle-event! "MESSAGE_CREATE"
  [_ evt state]
  (let [own-user (:own-user state)
        msg (get evt "id")
        guild (get evt "guild_id")
        channel (get evt "channel_id")
        author (get-in evt ["author" "id"])
        content (get evt "content")]
    (when-not (= author @own-user)
      (if (str/starts-with? content "!")
        (let [args (str/split (subs content 1) #" ")]
          (handle-command! (first args) (rest args) msg guild channel author state)
          #_(if @(:hangman-game state)
              (da/create-message! channel (str @(:hangman-game state)))
              (da/create-message! channel "no game atm")))
        (let [content-low (str/lower-case content)
              content-no-mentions (strip-mentions content)
              content-low-no-mentions (str/lower-case content-no-mentions)]
          (cond
            ; are you there?
            (and (mentioned? evt @own-user)
                 (str/includes? content-low "are you there"))
            (if @(:error-occurred state)
              (do
                (da/create-message! channel "I am, but an error occured! please check my log, para!")
                (reset! (:error-occurred state) nil))
              (da/create-message! channel "I am"))

            ; say hi
            (and (mentioned? evt @own-user)
                 (str/includes? content-low "say hi"))
            (da/create-message! channel "hi!")

            ; brb
            (or (some (partial str/includes? content-low) ["brb" "bbiaw"])
                (= content-low "back"))
            (da/create-message! channel (rand-nth ["ok"
                                                   "k"
                                                   "koé"]))

            ; allahu akbar
            (str/starts-with? content-low "allah")
            (da/create-message! channel "BOOM!")

            ; kaljatrol
            #_(= content-low "strol")
            #_(da/create-message! channel "kalja")
            
            ; reply to question
            (some (partial str/ends-with? content-low-no-mentions) ["?"
                                                                    "? lol"
                                                                    (str "? " e/laugh)])
            (if (yes-no-question? content-low-no-mentions)
              ; yes/no question
              (interject channel evt @own-user
                         (/ 1 20)
                         (-> [["yes"
                               "yeah"
                               "sure"
                               "certainly"
                               "of course"
                               "obviously"
                               "I believe so"
                               "I think so"
                               "I suppose so"]
                              ["no"
                               "nah"
                               "never"
                               "not a chance"
                               "of course not"
                               "obviously not"
                               "I believe not"
                               "I think not"
                               "I suppose not"]
                              ["maybe"
                               (str "maybe " e/laugh)
                               "I'm not sure"
                               "not sure"]]
                             (rand-nth)
                             (rand-nth)))
              (if (bad-aux-verb? content-low-no-mentions)
                ; diss about bad auxiliary verb use
                (interject channel evt @own-user
                           (/ 1 20)
                           "Shouldn't you learn grammar before speaking English?")
                ; other kind of question
                (interject channel evt @own-user
                           (/ 1 10)
                           (rand-nth ["lol I dunno"
                                      "dunno lol"
                                      "dunno"
                                      (str "dunno " e/laugh)]))))

            (looks-like-number? content-low)
            (interject channel evt @own-user
                       (/ 1 20)
                       (rand-nth ["That's numberwang!"
                                  "That's numberwang!"
                                  "That's wangernumb!"]))

            ; react to something funny
            #_
            (some (partial str/ends-with? content-low) ["lol" e/laugh])
            #_
            (interject channel evt @own-user
                       (/ 1 20)
                       (rand-nth [e/laugh
                                  "lol"
                                  "strol"]))

            ; hangman shortcut
            (and (= (count content-low) 1) ;; TODO make it look only at letters of the alphabet
                 @(:hangman-game state)
                 (= {:channel channel :user author} (:player @(:hangman-game state))))
            (handle-command! "guess" [content-low] nil guild channel author state)

            ; turdle "shortcut" (kinda but not really, as it's the only way to do a guess command in turdle)
            (and @(:turdle-game state)
                 (= {:channel channel :user author} (:player @(:turdle-game state)))
                 (trd/game-accepts-as-guess? @(:turdle-game state) content-low))
            (handle-turdle-guess! [content-low] channel author state)

            ; general interjection
            #_
            :else
            #_
            (interject channel evt @own-user
                       (/ 1 20)
                       "ye")))))))

(defmethod handle-event! "INTERACTION_CREATE"
  [_ evt state]
  (handle-interaction! (i/evt->interaction evt) state))