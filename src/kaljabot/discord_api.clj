(ns kaljabot.discord-api
  (:require [clj-http.client :as http]
            [clojure.data.json :as json]
            [kaljabot.util :as u]
            [kaljabot.constants :as c]))

(def base-url "https://discord.com/api/v10")
(def headers {"User-Agent" "DiscordBot (http://www.example.com, v1)"
              "Authorization" (str "Bot " c/auth-token)})

(defn request!
  ([f url body]
   (let [response (f (str base-url url)
                     {:headers headers
                      :body (when body (json/write-str body))
                      :content-type (when body :json)})]
     (when-let [response-body (:body response)]
       (json/read-str response-body))))
  ([f url]
   (request! f url nil)))

(defn get-guilds!
  []
  (request! http/get (u/compose-url "/users/%s/guilds"
                                    ["@me"])))

(defn get-guild-channels!
  [guild]
  (request! http/get (u/compose-url "/guilds/%s/channels"
                                    [guild])))

(defn search-guild-members!
  [guild query]
  (request! http/get (u/compose-url "/guilds/%s/members/search"
                                    [guild]
                                    {:query query})))

(defn get-messages!
  [channel]
  (request! http/get (u/compose-url "/channels/%s/messages"
                                    [channel])))

(defn get-gateway!
  []
  (request! http/get (u/compose-url "/gateway/bot"
                                    [])))

(defn create-message!
  [channel content]
  (request! http/post (u/compose-url "/channels/%s/messages"
                                     [channel])
            {"content" content}))

(defn edit-message!
  [channel message content]
  (request! http/patch (u/compose-url "/channels/%s/messages/%s"
                                      [channel message])
            {"content" content}))

(defn delete-message!
  [channel message]
  (request! http/delete (u/compose-url "/channels/%s/messages/%s"
                                       [channel message])))

(defn change-avatar!
  [media-type image-bytes]
  (request! http/patch (u/compose-url "/users/%s"
                                      ["@me"])
            {"avatar" (u/bytes->data-uri media-type image-bytes)}))

(defn get-user!
  [id]
  (request! http/get (u/compose-url "/users/%s"
                                    [id])))

(defn find-guild-by-name!
  [name]
  (-> (get-guilds!)
      (u/find-entry "name" name)
      (get "id")))

(defn find-channel-by-name!
  [guild name]
  (-> (get-guild-channels! guild)
      (u/find-entry "name" name)
      (get "id")))

(defn find-guild-user-by-name!
  [guild username]
  (-> (search-guild-members! guild username)
      (u/find-entry ["user" "username"] username)
      (get "user")
      (get "id")))

(defn create-reaction!
  [channel message emoji]
  (request! http/put (u/compose-url "/channels/%s/messages/%s/reactions/%s/@me"
                                    [channel message emoji])))

(defn delete-reaction!
  ([channel message emoji user]
   (request! http/delete (u/compose-url "/channels/%s/messages/%s/reactions/%s/%s"
                                        [channel message emoji user])))
  ([channel message emoji]
   (delete-reaction! channel message emoji "@me")))

(defn get-reactions!
  [channel message emoji]
  (request! http/get (u/compose-url "/channels/%s/messages/%s/reactions/%s"
                                    [channel message emoji])))

(defn delete-all-reactions!
  ([channel message emoji]
   (request! http/delete (u/compose-url "/channels/%s/messages/%s/reactions/%s"
                                        [channel message emoji])))
  ([channel message]
   (request! http/delete (u/compose-url "/channels/%s/messages/%s/reactions"
                                        [channel message]))))

(defn create-interaction-response!
  [id token type data]
  (request! http/post (u/compose-url "/interactions/%s/%s/callback"
                                     [id token])
            (if-not (nil? data)
              {"type" type
               "data" data}
              {"type" type})))

(defn get-global-commands!
  [app]
  (request! http/get (u/compose-url "/applications/%s/commands"
                                    [app])))

(defn create-global-command!
  [app command]
  (request! http/post (u/compose-url "/applications/%s/commands"
                                     [app])
            command))

(defn edit-global-command!
  [app id command]
  (request! http/patch (u/compose-url "/applications/%s/commands/%s"
                                      [app id])
            command))

(defn delete-global-command!
  [app id]
  (request! http/delete (u/compose-url "/applications/%s/commands/%s"
                                       [app id])))