(ns kaljabot.gateway
  (:require [clojure.data.json :as json]
            [gniazdo.core :as ws]
            [clojure.core.async :as async]
            [kaljabot.discord-api :as da]
            [kaljabot.constants :as c])
  (:import (java.util.concurrent ExecutionException)
           (org.eclipse.jetty.websocket.api WebSocketException)))

(defn compose-identify-msg
  []
  {:op 2
   :d {"token" c/auth-token
       "properties" {"$os" "windows"
                     "$browser" "kaljabot"
                     "$device" "kaljabot"}}})

(defn start-heartbeat!
  [gateway-send-ch sequence-number delay]
  (let [heartbeat-ch (async/chan 10)]
    (async/go-loop []
                   (let [[val ch] (async/alts! [heartbeat-ch (async/timeout delay)])]
                     (when-not (and (= ch heartbeat-ch) (nil? val))
                       (println (str "tick! " @sequence-number))
                       (async/>! gateway-send-ch {:op 1 :d @sequence-number})
                       (recur))))
    heartbeat-ch))

(def websocket (atom nil))

(defn- should-reconnect?
  [status reason]
  (or (and (= status 1001) (= reason "Discord WebSocket requesting client reconnect."))
      (and (= status 1006) (= reason "WebSocket Read EOF"))
      (and (= status 1006) (= reason "Disconnected"))))

(defn open-new-websocket!
  [gateway-send-ch gateway-recv-ch]
  (reset! websocket (ws/connect (get (da/get-gateway!) "url")
                                :on-connect (fn [_]
                                              (println (str "[CONNECTED]")))
                                :on-close   (fn [status reason]
                                              (println (str "[CLOSE " status " " reason "]"))
                                              (when (should-reconnect? status reason)
                                                (println "Will reconnect in 10 seconds.")
                                                (Thread/sleep 10000)
                                                (println "Reconnecting now...")
                                                (open-new-websocket! gateway-send-ch gateway-recv-ch)))
                                :on-error   (fn [msg]
                                              (println (str "[ERROR " msg "]")))
                                :on-receive (comp (fn [msg] (async/put! gateway-recv-ch msg))
                                                  json/read-str))))

(defn- send-msg-when-websocket-open
  [msg]
  (try
    (ws/send-msg @websocket msg)
    (catch ExecutionException ex
      (if (= (type (.getCause ex)) WebSocketException)
        (do
          (println "Message sending failed; will resend in 10 seconds.")
          (Thread/sleep 10000)
          (println "Resending message now...")
          (send-msg-when-websocket-open msg))
        (throw ex)))))

(defn start-gateway!
  []
  (let [gateway-send-ch (async/chan 10)
        gateway-recv-ch (async/chan 10)]
    (println "Connecting...")
    (open-new-websocket! gateway-send-ch gateway-recv-ch)
    (async/go-loop []
                   (if-let [msg (async/<! gateway-send-ch)]
                     (do
                       (println (str ">>> " msg))
                       (send-msg-when-websocket-open (json/write-str msg))
                       (recur))
                     (do
                       (ws/close @websocket))))
    [gateway-send-ch
     gateway-recv-ch]))
