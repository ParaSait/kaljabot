(ns kaljabot.command-constants)

(def cmd-type-chat-input 1)
(def cmd-type-user 2)
(def cmd-type-message 3)

(def opt-type-sub-command 1)
(def opt-type-sub-command-group 2)
(def opt-type-string 3)
(def opt-type-integer 4)
(def opt-type-boolean 5)
(def opt-type-user 6)
(def opt-type-channel 7)
(def opt-type-role 8)
(def opt-type-mentionable 9)
(def opt-type-number 10)
(def opt-type-attachment 11)