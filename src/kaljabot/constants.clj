(ns kaljabot.constants)

(def application-id (System/getenv "APPLICATION_ID"))
(def auth-token (System/getenv "AUTH_TOKEN"))
(def os-name (System/getenv "OS_NAME"))

(def discord-char-limit 2000)

(def hangman-scores-path "data/hangman_scores.json")
(def turdle-scores-path "data/turdle_scores.json")
(def turdle-modes-path "resources/turdle_modes.json")
(def user-prefs-path "data/user_prefs.json")

(def version-file-path "version.txt")
