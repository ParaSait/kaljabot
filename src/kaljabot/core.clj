(ns kaljabot.core
  (:gen-class)
  (:require [clojure.core.async :as async]
            [kaljabot.gateway :as gw]
            [kaljabot.bot :as b]))

(defn start-client!
  []
  (let [[gateway-send-ch
         gateway-recv-ch] (gw/start-gateway!)
        state {:heartbeat-ch (atom nil)
               :sequence-number (atom nil)
               :session-id (atom nil)
               :own-user (atom nil)
               :hangman-game (atom nil)
               :turdle-game (atom nil)
               :error-occurred (atom nil)}]
    (async/go-loop []
      (if-let [msg (async/<! gateway-recv-ch)]
        (do
          (try
            (let [{op "op"
                   d "d"
                   s "s"
                   t "t"} msg]
              (do
                (println (str "<<< " msg))
                (when s (reset! (:sequence-number state) s))
                (case op
                  0 (b/handle-event! t d state)
                  10 (do
                       (reset! (:heartbeat-ch state)
                               (gw/start-heartbeat! gateway-send-ch
                                                    (:sequence-number state)
                                                    (get d "heartbeat_interval")))
                       (async/>! gateway-send-ch (gw/compose-identify-msg)))
                  11 (println "ack!")
                  (println (str "unhandled code: " op)))))
            (catch Exception ex
              (println "unhandled exception in client thread at bot state:")
              (clojure.pprint/pprint (into {} (map (fn [[k v]] [k @v]) state)))
              (.printStackTrace ex)
              (reset! (:error-occurred state) true)))
          (recur))
        (do
          (async/close! gateway-send-ch)
          (when @(:heartbeat-ch state)
            (async/close! @(:heartbeat-ch state))))))

    gateway-recv-ch))

(defn -main
  []
  (start-client!))
