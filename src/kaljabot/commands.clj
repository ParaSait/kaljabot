(ns kaljabot.commands
  (:require [kaljabot.command-constants :as cc]))

(def cmd-version
  {:name "version"
   :type cc/cmd-type-chat-input
   :description "Query Kaljabot's currently active version"})

(def cmd-roll
  {:name "roll"
   :type cc/cmd-type-chat-input
   :description "Do a dice roll"
   :options [{:name "dice"
              :description "e.g. '6', or '2d6'"
              :type cc/opt-type-string}]})

(def cmd-bullshit
  {:name "bullshit"
   :type cc/cmd-type-chat-input
   :description "Generate some bullshit"
   :options [{:name "fg"
              :description "The 'foreground' tile, such as a pile of bullshit"
              :type cc/opt-type-string
              :required true}
             {:name "bg"
              :description "The 'background' tile, such as a pile of horseshit"
              :type cc/opt-type-string
              :required true}
             {:name "text"
              :description "The text to be bullshittified (don't make it too long, or Discord will choke on it lol)"
              :type cc/opt-type-string
              :required true}]})

(def cmd-hangman
  {:name "hangman"
   :type cc/cmd-type-chat-input
   :description "Play a game of Hangman"})

(def cmd-guess
  {:name "guess"
   :type cc/cmd-type-chat-input
   :description "Guess the word in Hangman"
   :options [{:name "word"
              :description "The word (or letter, if it's just a single character)"
              :type cc/opt-type-string
              :required true}]})

(def turdle-mode-choices
  [{:name "wordle"
    :value "wordle"}
   {:name "wordle6"
    :value "wordle6"}
   {:name "wrdl"
    :value "wrdl"}
   {:name "wordle-hard"
    :value "wordle-hard"}
   {:name "wordle-strict"
    :value "wordle-strict"}])

(def turdle-mode-parameter
  {:name "mode"
   :type cc/opt-type-string
   :description "Game mode"
   :choices turdle-mode-choices})

(def cmd-turdle
  {:name "turdle"
   :type cc/cmd-type-chat-input
   :description "Turdle, the Wordle clone for Discord!"
   :options [{:name "play"
              :type cc/opt-type-sub-command
              :description "Play a game of Turdle"
              :options [turdle-mode-parameter]}
             {:name "scores"
              :type cc/opt-type-sub-command
              :description "Show a player's scores"
              :options [turdle-mode-parameter
                        {:name "player"
                         :type cc/opt-type-user
                         :description "Player"}]}
             {:name "streaks"
              :type cc/opt-type-sub-command
              :description "Show a player's streaks"
              :options [turdle-mode-parameter]}
             {:name "modes"
              :type cc/opt-type-sub-command
              :description "List game modes"}]})

(def pref-key-parameter
  {:name "key"
   :type cc/opt-type-string
   :description "Preference key"
   :choices [{:name "turdle-mode"
              :value "turdle-mode"}]
   :required true})

(def cmd-pref
  {:name "pref"
   :type cc/cmd-type-chat-input
   :description "Set your preferences"
   :options [{:name "set"
              :type cc/opt-type-sub-command
              :description "Set a preference"
              :options [pref-key-parameter
                        {:name "value"
                         :type cc/opt-type-string
                         :description "Preference value"
                         :choices turdle-mode-choices ;; TODO a bit of a hack, since there's only one available option so far; need to make the keys into subcommands
                         :required true}]}
             {:name "get"
              :type cc/opt-type-sub-command
              :description "Get a preference"
              :options [pref-key-parameter]}]})

(def all-cmds
  [cmd-version
   cmd-roll
   cmd-bullshit
   cmd-hangman
   cmd-guess
   cmd-turdle
   cmd-pref])