(ns kaljabot.interaction
  (:require [kaljabot.discord-api :as da]
            [kaljabot.command-constants :as cc]))

(def type-pong 1)
(def type-channel-message-with-source 4)
(def type-deferred-channel-message-with-source 5)
(def type-deferred-update-message 6)
(def type-update-message 7)
(def type-application-command-autocomplete-result 8)
(def type-modal 9)

(defn evt->interaction
  [evt]
  (select-keys evt ["id" "token" "guild_id" "channel_id" "data" "member"]))

(defn id
  [interaction]
  (get interaction "id"))

(defn token
  [interaction]
  (get interaction "token"))

(defn guild
  [interaction]
  (get interaction "guild_id"))

(defn channel
  [interaction]
  (get interaction "channel_id"))

(defn data
  [interaction]
  (get interaction "data"))

(defn command-name
  [command]
  (get command "name"))

(defn command-opts
  [command]
  (->> (get command "options")
       (map (fn [opt]
              [(keyword (get opt "name"))
               (cond
                 (= (get opt "type") cc/opt-type-sub-command) (command-opts opt)
                 :else (get opt "value"))]))
       (into {})))

(defn user-id
  [interaction]
  (get-in interaction ["member" "user" "id"]))

(defn ->response
  [type data]
  {:type type
   :data data})

(defn ->message-response
  [content]
  (->response type-channel-message-with-source
              {"content" content}))

(defn respond
  [response interaction]
  (let [{type :type data :data} response]
    (da/create-interaction-response! (id interaction) (token interaction) type data)))