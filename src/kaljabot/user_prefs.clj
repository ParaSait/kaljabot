(ns kaljabot.user-prefs
  (:require [kaljabot.constants :as c]
            [kaljabot.util :as u]
            [kaljabot.turdle :as trd]))

(def load-user-prefs-data!
  (partial u/read-json-from-file! c/user-prefs-path))

(def save-user-prefs-data!
  (partial u/write-json-to-file! c/user-prefs-path))

(defn ->default-user-prefs
  []
  {"turdle-mode" "wordle"})

(defn- get-valid-turdle-modes
  []
  (-> (trd/load-mode-data!)
      (keys)
      (set)))

(defn get-user-prefs-pattern
  []
  {"turdle-mode" (fn [value] ((get-valid-turdle-modes) value))})

(defn- get-valid-keys
  []
  (-> (get-user-prefs-pattern)
      (keys)
      (set)))

(defn assert-key-valid!
  [key]
  (when-not ((get-valid-keys) key)
    (throw (ex-info "Invalid user pref key" {:type :invalid-user-pref-key
                                             :key  key}))))

(defn assert-value-valid!
  [key value]
  (when-not ((get (get-user-prefs-pattern) key) value)
    (throw (ex-info "Invalid user pref value" {:type  :invalid-user-pref-value
                                               :key   key
                                               :value value}))))

(defn set-user-pref!
  [user key value]
  (assert-key-valid! key)
  (assert-value-valid! key value)
  (-> (load-user-prefs-data!)
      (update user #(assoc (or % (->default-user-prefs)) key value))
      (save-user-prefs-data!)))

(defn get-user-pref!
  [user key]
  (assert-key-valid! key)
  (or (get-in (load-user-prefs-data!) [user key])
      (get (->default-user-prefs) key)))