(ns kaljabot.emoji
  (:require [kaljabot.util :as u]))

(def laugh (u/unicode 128516))
(def clap (u/unicode 128079))

(def heyhey "<:heyhey:410256115646070784>")
(def elon "<:elon:561539999091130378>")
(def aratilt "<:AraTilt:411998198735831041>")
(def mike "<:mike:533708253176135712>")
(def musk "<:musk:505477862451838976>")
(def schoppy "<:schoppy:588102792128888858>")
(def bullshit "<:bullshit:410356466638127105>")
(def dd "<:dd:412002684691021824>")
(def menri "<:menri:496132935456653322>")
(def dovahkiin "<:dovahkiin:423146953434726402>")