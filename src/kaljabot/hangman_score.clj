(ns kaljabot.hangman-score
  (:require [kaljabot.constants :as c]
            [kaljabot.util :as u]))

(def load-score-data!
  (partial u/read-json-from-file! c/hangman-scores-path))

(def save-score-data!
  (partial u/write-json-to-file! c/hangman-scores-path))

(defn tally-score!
  [user rank]
  (let [scores  (load-score-data!)
        scores* (update-in scores [user rank] (fnil inc 0))]
    (save-score-data! scores*)
    scores*))

(defn format-user-scores-for-message
  [score-data user]
  (str "["
       (->> ["DOVAHKIIN" "S+" "S" "A" "B" "C" "D" "E" "F" "dead"]
            (map (fn [rank]
                   (when-let [score (get-in score-data [user rank])]
                     (str rank ": " score))))
            (remove nil?)
            (clojure.string/join " | "))
       "]"))