(ns kaljabot.turdle-score
  (:require [kaljabot.constants :as c]
            [kaljabot.util :as u]))

(def load-score-data!
  (partial u/read-json-from-file! c/turdle-scores-path))

(def save-score-data!
  (partial u/write-json-to-file! c/turdle-scores-path))

(defn- ->new-stats
  [{max-guesses :max-guesses}]
  {"scores"      (vec (repeat (inc max-guesses) 0))
   "streak"      0
   "best-streak" 0})

(defn- increment-score-in-stats
  [stats score]
  (let [streak (if (= score 0)
                 0
                 (inc (get stats "streak")))]
    (-> stats
        (update-in ["scores" score] inc)
        (assoc      "streak"        streak)
        (update     "best-streak"   (partial max streak)))))

(defn tally-score!
  [mode mode-attrs user score]
  (let [scores  (load-score-data!)
        scores* (update-in scores [mode user] #(increment-score-in-stats (or % (->new-stats mode-attrs)) score))]
    (save-score-data! scores*)
    scores*))

(defn query-streak-overview-info
  [score-data mode]
  (->> (get score-data mode)
       (map (fn [[user user-data]]
              {user {:streak (get user-data "streak")
                     :best-streak (get user-data "best-streak")}}))
       (into {})))

(defn query-overall-best-streak-info
  [score-data mode]
  (let [overview-info (query-streak-overview-info score-data mode)]
    (if-not (empty? overview-info)
      (let [highest (->> overview-info
                         (map (fn [[_ {best-streak :best-streak}]] best-streak))
                         (apply max))
            users   (->> overview-info
                         (filter (fn [[_ {best-streak :best-streak}]] (= best-streak highest)))
                         (map (fn [[user _]] user))
                         (vec))]
        [highest users]))))

(defn find-stats-for-user
  [score-data mode user]
  (get-in score-data [mode user]))

(defn query-times-played-per-mode
  [score-data]
  (->> score-data
       (map (fn [[mode mode-data]]
              [mode
               (->> mode-data
                    (vals)
                    (map #(get % "scores"))
                    (flatten)
                    (apply +))]))
       (into {})))