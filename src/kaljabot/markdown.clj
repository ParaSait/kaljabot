(ns kaljabot.markdown)

(defn- surround
  [circumfix content]
  (if-not (clojure.string/blank? (str content))
    (str circumfix content circumfix)
    ""))

(def bold          (partial surround "**"))
(def italic        (partial surround "_"))
(def underline     (partial surround "__"))
(def strikethrough (partial surround "~~"))
(def monospace     (partial surround "`"))
(def codeblock     (partial surround "```"))

(defn multiquote
  [content]
  (if-not (clojure.string/blank? (str content))
    (str ">>> " content)
    ""))