(ns kaljabot.cah
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(def ^{:private true} black-cards-path "data/cah_black_cards.txt")
(def ^{:private true} white-cards-path "data/cah_white_cards.txt")

(def ^{:private true} msg-joined-game "You've joined a game of Cards Against Humanity.")
(def ^{:private true} msg-game-started "The game of Cards Against Humanity has started.")
(def ^{:private true} msg-not-in-game "You're not in the game, so you can't start it.")
(def ^{:private true} msg-game-already-ongoing "A game is currently already ongoing.")
(def ^{:private true} msg-not-enough-players "There are not enough players to start the game.")

(defn- load-black-cards!
  []
  (if (.isFile (io/file black-cards-path))
    (str/split-lines (slurp black-cards-path))
    []))

(defn- add-black-card!
  [black-card]
  (->> (str (slurp black-cards-path) black-card "\n")
       (spit black-cards-path)))

(defn- load-white-cards!
  []
  (if (.isFile (io/file white-cards-path))
    (str/split-lines (slurp white-cards-path))
    []))

(defn- add-white-card!
  [white-card]
  (->> (str (slurp white-cards-path) white-card "\n")
       (spit white-cards-path)))

(defn ->initial-game
  [msg-f dm-f]
  {:msg-f msg-f
   :dm-f dm-f
   :players []
   :ongoing false})

(defn send-msg!
  [game msg]
  (:msg-f game) msg)

(defn with-msg!
  [msg game]
  (send-msg! game msg)
  game)

(defn send-dm!
  [game player msg]
  (:dm-f game) player msg)

(defn with-dm!
  [player msg game]
  (send-dm! game player msg)
  game)

(def ^{:private true} is-ongoing?
  :ongoing)

(defn- is-in-game?
  [game player]
  (some (partial = player)
        (:players game)))

(defn- enough-players?
  [game]
  (>= (count (:player game)) 3))

(defn do-join!
  [game player]
  (with-dm! player msg-joined-game
    (update game :players #(assoc % player))))

(defn do-start!
  [game player]
  (if (is-ongoing? game)
    (with-dm! player msg-game-already-ongoing game)
    (if-not (is-in-game? game player)
      (with-dm! player msg-not-in-game game)
      (if-not (enough-players? game)
        (with-dm! player msg-not-enough-players game)
        (with-msg! msg-game-started
          (assoc game :ongoing true))))))