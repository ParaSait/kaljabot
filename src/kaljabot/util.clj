(ns kaljabot.util
  (:require [clojure.string :as str]
            [clojure.walk :as w]
            [clojure.java.io :as io]
            [clojure.data.json :as json]
            [clojure.data.codec.base64 :as base64]
            [ring.util.codec :as codec]))

(defn find-first
  [f coll]
  (first (filter f coll)))

(defn find-entry
  [coll key val]
  (find-first (fn [h] (= val ((if (vector? key) get-in get) h key))) coll))

(def ^{:private true} encode-url-param
  (comp codec/url-encode
        (fnil identity "")))

(defn compose-url
  ([route pos-params url-params]
   (let [route* (apply format route (map encode-url-param pos-params))]
     (if-not (empty? url-params)
       (str route* "?" (str/join "&" (map (fn [entry]
                                            (str/join "=" (map encode-url-param entry)))
                                          (w/stringify-keys url-params))))
       route*)))
  ([route pos-params]
   (compose-url route pos-params {})))

(defn unicode
  [n]
  (String. (Character/toChars n)))

(defn slurp-bytes
  [x]
  (with-open [out (java.io.ByteArrayOutputStream.)]
    (clojure.java.io/copy (clojure.java.io/input-stream x) out)
    (.toByteArray out)))

(defn bytes->data-uri
  [media-type bytes]
  (str "data:" media-type ";base64," (String. (base64/encode bytes))))

(defn is-letter?
  [^Character x]
  (Character/isLetter x))

(defn read-json-from-file!
  [path]
  (when (.exists (io/file path))
    (with-open [reader (io/reader path)]
      (json/read reader))))

(defn write-json-to-file!
  [path score-data]
  (io/make-parents path)
  (with-open [writer (io/writer path)]
    (json/write score-data writer)))