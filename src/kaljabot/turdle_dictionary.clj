(ns kaljabot.turdle-dictionary
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(def ^{:private true} turdle-words-path "data/turdle_words.txt")
(def ^{:private true} hangman-words-path "resources/hangman_words.txt")

(defn- bootstrap-dictionary!
  []
  (->> (slurp hangman-words-path)
       (spit turdle-words-path)))

(defn load-dictionary!
  []
  (when-not (.isFile (io/file turdle-words-path))
    (bootstrap-dictionary!))
  (str/split-lines (slurp turdle-words-path)))

(defn contains-word?
  [word]
  (some (partial = word)
        (load-dictionary!)))

(defn add-word!
  [word]
  (->> (str (slurp turdle-words-path) word "\n")
       (spit turdle-words-path)))