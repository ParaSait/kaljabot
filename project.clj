(defproject kaljabot "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.3"]
                 [clj-http "3.12.3"]
                 [ring/ring-codec "1.2.0"]
                 [slingshot "0.12.2"]
                 [org.clojure/data.json "2.4.0"]
                 [stylefruits/gniazdo "1.2.0"]
                 [org.clojure/core.async "1.3.618"]
                 [org.clojure/data.codec "0.1.1"]
                 [org.xerial/sqlite-jdbc "3.36.0.1"]
                 [korma "0.4.3"]]
  :profiles {:dev {:dependencies [[midje "1.10.3"]]
                   :plugins [[lein-midje "3.2.1"]]}
             :uberjar {:main        kaljabot.core
                       :aot         :all
                       :omit-source true}})
