(ns kaljabot.command-management-test
  (:require [midje.sweet :refer :all]
            [kaljabot.command-management :as mgmt]
            [kaljabot.discord-api :as da]))

(facts "about command-diff"
       (fact "determines commands that are to be created, updated and deleted"
             (mgmt/command-diff [{:name "foo" :type 1}
                                 {:name "bar" :type 2}
                                 {:name "qux" :type 1}]
                                [{:id "2" :name "bar" :type 1}
                                 {:id "3" :name "baz" :type 1}
                                 {:id "4" :name "qux" :type 1}])
             => (just [[:create {:name "foo" :type 1}]
                       [:update {:id "2" :name "bar" :type 2}]
                       [:delete {:id "3" :name "baz" :type 1}]]
                      :in-any-order))

       (fact "an update is not necessary if current command is exactly the same as target command"
             (mgmt/command-diff [{:name "foo" :type 1}]
                                [{:id "1" :name "foo" :type 1}])
             => empty?)

       (fact "an update is necessary if target command has an attribute that differs from the current command"
             (mgmt/command-diff [{:name "foo" :type 2}]
                                [{:id "1" :name "foo" :type 1}])
             => [[:update {:id "1" :name "foo" :type 2}]])

       (fact "an update is necessary if the target command specifies something that the target command doesn't have"
             (mgmt/command-diff [{:name "foo" :type 1 :description "Foo"}]
                                [{:id "1" :name "foo" :type 1}])
             => [[:update {:id "1" :name "foo" :type 1 :description "Foo"}]])

       (fact "an update is not necessary if the current command has something that the target command doesn't"
             (mgmt/command-diff [{:name "foo" :type 1}]
                                [{:id "1" :name "foo" :type 1 :description "Foo"}])
             => empty?))

(facts "about transact-diff"
       (fact "transacts a command diff"
             (mgmt/transact-diff! ..app..
                                  [[:create {:name "foo" :type 1}]
                                   [:update {:id "2" :name "bar" :type 2}]
                                   [:delete {:id "3" :name "baz" :type 1}]])
             => nil
             (provided
              (da/create-global-command! ..app.. {"name" "foo" "type" 1})
              => nil :times 1
              (da/edit-global-command! ..app.. "2" {"name" "bar" "type" 2})
              => nil :times 1
              (da/delete-global-command! ..app.. "3")
              => nil :times 1)))