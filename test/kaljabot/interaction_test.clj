(ns kaljabot.interaction-test
  (:require [midje.sweet :refer :all]
            [kaljabot.interaction :as i]
            [kaljabot.command-constants :as cc]))

(def test-evt
  {"id" "123"
   "token" "inserttokenhere"
   "guild_id" "321"
   "channel_id" "456"
   "data" {"name" "testcmd"
           "type" cc/cmd-type-chat-input
           "options" [{"name" "foo"
                       "type" cc/opt-type-string
                       "value" "bar"}
                      {"name" "baz"
                       "type" cc/opt-type-string
                       "value" "qux"}]}
   "member" {"user" {"id" "789"}}})

(def test-subcommand-evt
  {"data" {"name" "testcmd"
           "type" cc/cmd-type-chat-input
           "options" [{"name" "scores"
                       "type" cc/opt-type-sub-command
                       "options" [{"value" "12345"
                                   "type" cc/opt-type-user
                                   "name" "player"}]}]}})

(facts "about id"
       (fact "gets the id of the interaction"
             (i/id (i/evt->interaction test-evt))
             => "123"))

(facts "about token"
       (fact "gets the token associated with the interaction"
             (i/token (i/evt->interaction test-evt))
             => "inserttokenhere"))

(facts "about guild"
       (fact "gets the guild associated with the interaction"
             (i/guild (i/evt->interaction test-evt))
             => "321"))

(facts "about channel"
       (fact "gets the channel associated with the interaction"
             (i/channel (i/evt->interaction test-evt))
             => "456"))

(facts "about data"
       (fact "gets the command data of the interaction"
             (i/data (i/evt->interaction test-evt))
             => {"name" "testcmd"
                 "type" cc/cmd-type-chat-input
                 "options" [{"name" "foo"
                             "type" cc/opt-type-string
                             "value" "bar"}
                            {"name" "baz"
                             "type" cc/opt-type-string
                             "value" "qux"}]}))

(facts "about command-name"
       (fact "gets the name from command data"
             (i/command-name (i/data (i/evt->interaction test-evt)))
             => "testcmd"))

(facts "about command-args"
       (fact "gets the map of options for the command"
             (i/command-opts (i/data (i/evt->interaction test-evt)))
             => {:foo "bar"
                 :baz "qux"})

       (fact "gets the map of options for a command with subcommands"
             (i/command-opts (i/data (i/evt->interaction test-subcommand-evt)))
             => {:scores {:player "12345"}}))

(facts "about user-id"
       (fact "gets the id of the user that's interacting"
             (i/user-id (i/evt->interaction test-evt))
             => "789"))