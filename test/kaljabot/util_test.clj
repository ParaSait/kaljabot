(ns kaljabot.util-test
  (:require [midje.sweet :refer :all]
            [kaljabot.util :as u]))

(facts "about find-first"
       (fact "finds first match in coll"
             (u/find-first even? [1 2 3 4]) => 2)

       (fact "nil if no match"
             (u/find-first even? [1 3 5]) => nil)

       (fact "nil if coll is empty"
             (u/find-first even? []) => nil)

       (fact "nil if coll is nil"
             (u/find-first even? nil) => nil))

(facts "about find-entry"
       (fact "finds an entry in a map"
             (u/find-entry [{:a :x} {:b :x} {:a :y} {:b :y}] :b :y) => {:b :y})

       (fact "finds a nested entry if the key is a vector"
             (u/find-entry [{:a {:e :x}} {:b {:e :x}} {:a {:e :y}} {:b {:f :y}}] [:b :e] :x) => {:b {:e :x}})

       (fact "nil if nothing found"
             (u/find-entry [{:a :x} {:b :x} {:a :y} {:b :y}] :c :x) => nil)

       (fact "nil if coll is empty"
             (u/find-entry [] :a :x) => nil)

       (fact "nil if coll is nil"
             (u/find-entry nil :a :x) => nil))

(facts "about compose-url"
       (fact "without params"
             (u/compose-url "http://www.example.com/" [] {}) => "http://www.example.com/")

       (fact "with one param"
             (u/compose-url "http://www.example.com/" [] {:a 1}) => "http://www.example.com/?a=1")

       (fact "with more than one param"
             (u/compose-url "http://www.example.com/" [] {:a 1 "b" "lol"}) => "http://www.example.com/?a=1&b=lol")

       (fact "nil params give empty values"
             (u/compose-url "http://www.example.com/" [] {:a nil}) => "http://www.example.com/?a=")

       (fact "params are percent-encoded"
             (u/compose-url "http://www.example.com/" [] {"a!" "foo#bar$baz%qux"}) => "http://www.example.com/?a%21=foo%23bar%24baz%25qux")

       (fact "positional parameters"
             (u/compose-url "http://www.example.com/foo/%s/bar/%s" ["qux" 123] {}) => "http://www.example.com/foo/qux/bar/123")
       
       (fact "positional parameters combined with url parameters"
             (u/compose-url "http://www.example.com/foo/%s/bar/%s" ["qux" 123] {:a "b"}) => "http://www.example.com/foo/qux/bar/123?a=b")

       (fact "positional parameters are percent-encoded"
             (u/compose-url "http://www.example.com/emoji/%s" ["<:bullshit:410356466638127105>"] {}) => "http://www.example.com/emoji/%3C%3Abullshit%3A410356466638127105%3E"))

(def ^{:private true} test-json-file
  "test/resources/test_turdle_scores.json")

(facts "about read-json-from-file!"
       (fact "reads json data from a file if it exists"
             (u/read-json-from-file! test-json-file)
             => {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                       "streak" 1
                                       "best-streak" 11}}
                 "nerdle" {"ParaSait" {"scores" [102 21 22 23 24 25 26]
                                       "streak" 2
                                       "best-streak" 12}
                           "nyasudah" {"scores" [103 31 32 33 34 35 36]
                                       "streak" 3
                                       "best-streak" 13}}
                 "quordle" {"nyasudah" {"scores" [104 41 42 43 44 45 46]
                                        "streak" 4
                                        "best-streak" 14}}})
       
       (fact "returns nil if the file doesn't exist"
             (u/read-json-from-file! "bogus.json")
             => nil?))