(ns kaljabot.user-prefs-test
  (:require [midje.sweet :refer :all]
            [kaljabot.user-prefs :as pref]))

(facts "about set-user-pref!"
       (fact "sets the value of a key for a user"
             (pref/set-user-pref! "ParaSait" "foo" "qux")
             => irrelevant
             (provided
              (pref/load-user-prefs-data!)
              => {"ParaSait" {"foo" "bar"}}
              :times 1
              (pref/get-user-prefs-pattern)
              => {"foo" (constantly true)}
              (pref/save-user-prefs-data!
               {"ParaSait" {"foo" "qux"}})
              => irrelevant
              :times 1))
       
       (fact "creates a key if it doesn't exist yet"
             (pref/set-user-pref! "ParaSait" "foo" "qux")
             => irrelevant
             (provided
              (pref/load-user-prefs-data!)
              => {"ParaSait" {"baz" "quux"}}
              :times 1
              (pref/get-user-prefs-pattern)
              => {"foo" (constantly true)}
              (pref/save-user-prefs-data!
               {"ParaSait" {"baz" "quux"
                            "foo" "qux"}})
              => irrelevant
              :times 1))
       
       (fact "creates a user with default settings if it doesn't exist yet"
             (pref/set-user-pref! "ParaSait" "foo" "qux")
             => irrelevant
             (provided
              (pref/load-user-prefs-data!)
              => {"nyasudah" {"foo" "bar"}}
              :times 1
              (pref/->default-user-prefs)
              => {"baz" "quux"
                  "foo" "bar"}
              (pref/get-user-prefs-pattern)
              => {"foo" (constantly true)}
              (pref/save-user-prefs-data!
               {"nyasudah" {"foo" "bar"}
                "ParaSait" {"baz" "quux"
                            "foo" "qux"}})
              => irrelevant
              :times 1))
       
       (fact "creates a user prefs file if it doesn't exist yet"
             (pref/set-user-pref! "ParaSait" "foo" "qux")
             => irrelevant
             (provided
              (pref/load-user-prefs-data!)
              => nil
              :times 1
              (pref/->default-user-prefs)
              => {"baz" "quux"
                  "foo" "bar"}
              (pref/get-user-prefs-pattern)
              => {"foo" (constantly true)}
              (pref/save-user-prefs-data!
               {"ParaSait" {"baz" "quux"
                            "foo" "qux"}})
              => irrelevant
              :times 1))
       
       (fact "throws an exception if the key is not valid"
             (pref/set-user-pref! "ParaSait" "foo" "qux")
             => (throws clojure.lang.ExceptionInfo)
             (provided
              (pref/load-user-prefs-data!)
              => irrelevant
              :times 0
              (pref/get-user-prefs-pattern)
              => {"baz" (constantly true)}
              (pref/save-user-prefs-data! anything)
              => irrelevant
              :times 0))

       (fact "throws an exception if the value is not valid"
             (pref/set-user-pref! "ParaSait" "foo" "qux")
             => (throws clojure.lang.ExceptionInfo)
             (provided
              (pref/load-user-prefs-data!)
              => irrelevant
              :times 0
              (pref/get-user-prefs-pattern)
              => {"foo" (constantly false)}
              (pref/save-user-prefs-data! anything)
              => irrelevant
              :times 0)))

(facts "about get-user-pref!"
       (fact "gets the value of a key for a user"
             (pref/get-user-pref! "ParaSait" "foo")
             => "bar"
             (provided
              (pref/load-user-prefs-data!)
              => {"ParaSait" {"foo" "bar"}}
              (pref/get-user-prefs-pattern)
              => {"foo" (constantly true)}
              (pref/save-user-prefs-data!)
              => irrelevant
              :times 0))
       
       (fact "returns the default value for the key if it doesn't exist yet"
             (pref/get-user-pref! "ParaSait" "foo")
             => "bar"
             (provided
              (pref/load-user-prefs-data!)
              => {"ParaSait" {"bar" "qux"}}
              (pref/->default-user-prefs)
              => {"foo" "bar"}
              (pref/get-user-prefs-pattern)
              => {"foo" (constantly true)}
              (pref/save-user-prefs-data!)
              => irrelevant
              :times 0))
       
       (fact "returns the default value for the key if the user doesn't exist yet"
             (pref/get-user-pref! "ParaSait" "foo")
             => "bar"
             (provided
              (pref/load-user-prefs-data!)
              => {"nyasudah" {"bar" "qux"}}
              (pref/->default-user-prefs)
              => {"foo" "bar"}
              (pref/get-user-prefs-pattern)
              => {"foo" (constantly true)}
              (pref/save-user-prefs-data!)
              => irrelevant
              :times 0))
       
       (fact "returns the default value for the key if the user prefs file doesn't exist yet"
             (pref/get-user-pref! "ParaSait" "foo")
             => "bar"
             (provided
              (pref/load-user-prefs-data!)
              => nil
              :times 1
              (pref/->default-user-prefs)
              => {"foo" "bar"}
              (pref/get-user-prefs-pattern)
              => {"foo" (constantly true)}
              (pref/save-user-prefs-data!)
              => irrelevant
              :times 0))
       
       (fact "throws an exception if the key is not valid"
             (pref/get-user-pref! "ParaSait" "foo")
             => (throws clojure.lang.ExceptionInfo)
             (provided
              (pref/load-user-prefs-data!)
              => irrelevant
              :times 0
              (pref/get-user-prefs-pattern)
              => {"baz" (constantly true)}
              (pref/save-user-prefs-data!)
              => irrelevant
              :times 0)))