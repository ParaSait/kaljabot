(ns kaljabot.hangman-score-test
  (:require [midje.sweet :refer :all]
            [kaljabot.hangman-score :as hs]
            [kaljabot.constants :as c]))

(facts "about tally-score!"
       (fact "increments a tally count of one of a particular player's ranks in the score file, and returns the new scores"
             (let [old-scores {"ParaSait" {"A" 1
                                           "B" 2
                                           "C" 3}
                               "nyasudah" {"A" 4
                                           "B" 5
                                           "C" 6}}
                   new-scores {"ParaSait" {"A" 2
                                           "B" 2
                                           "C" 3}
                               "nyasudah" {"A" 4
                                           "B" 5
                                           "C" 6}}]
               (hs/tally-score! "ParaSait" "A")
               => new-scores
               (provided
                (hs/load-score-data!)
                => old-scores :times 1
                (hs/save-score-data! new-scores)
                => nil :times 1)))
       
       (fact "creates a rank when nonexistent for a user"
             (let [old-scores {"ParaSait" {"B" 2
                                           "C" 3}
                               "nyasudah" {"A" 4
                                           "B" 5
                                           "C" 6}}
                   new-scores {"ParaSait" {"A" 1
                                           "B" 2
                                           "C" 3}
                               "nyasudah" {"A" 4
                                           "B" 5
                                           "C" 6}}]
               (hs/tally-score! "ParaSait" "A")
               => new-scores
               (provided
                (hs/load-score-data!)
                => old-scores :times 1
                (hs/save-score-data! new-scores)
                => nil :times 1)))
       
       (fact "creates a user with an initial rank when nonexistent"
             (let [old-scores {"nyasudah" {"A" 4
                                           "B" 5
                                           "C" 6}}
                   new-scores {"ParaSait" {"A" 1}
                               "nyasudah" {"A" 4
                                           "B" 5
                                           "C" 6}}]
               (hs/tally-score! "ParaSait" "A")
               => new-scores
               (provided
                (hs/load-score-data!)
                => old-scores :times 1
                (hs/save-score-data! new-scores)
                => nil :times 1)))
       
       (fact "creates an initial user with an initial rank when the scores themselves are nonexistent"
             (let [old-scores nil
                   new-scores {"ParaSait" {"A" 1}}]
               (hs/tally-score! "ParaSait" "A")
               => new-scores
               (provided
                (hs/load-score-data!)
                => old-scores :times 1
                (hs/save-score-data! new-scores)
                => nil :times 1))))

(facts "about format-user-scores-for-message"
       (fact "formats a score string for the given user"
             (hs/format-user-scores-for-message
              {"ParaSait" {"A" 1
                           "B" 2
                           "C" 3
                           "dead" 4
                           "bogus" 5}
               "nyasudah" {"A" 4
                           "B" 5
                           "C" 6}}
              "ParaSait")
             => "[A: 1 | B: 2 | C: 3 | dead: 4]"))