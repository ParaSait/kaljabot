(ns kaljabot.turdle-test
  (:require [kaljabot.turdle :as trd]
            [kaljabot.turdle-score :as ts]
            [kaljabot.emoji :as e]
            [kaljabot.util :as u]
            [kaljabot.constants :as c]
            [midje.sweet :refer :all]))

(facts "about load-mode-data!"
       (fact "reads mode data from the turdle modes file, keywordizes mode attributes"
             (trd/load-mode-data!)
             => {"wordle" {:word-length 5
                           :max-guesses 6}}
             (provided
              (u/read-json-from-file! c/turdle-modes-path)
              => {"wordle" {"word-length" 5
                            "max-guesses" 6}})))

(facts "about pickable-word?"
       (fact "word is pickable if the length is right"
             (trd/pickable-word? 5 "strol") => truthy
             (trd/pickable-word? 5 "bismus") => falsey)

       (fact "word is not pickable if it contains non-alphabetical characters"
             (trd/pickable-word? 5 "x-ray") => falsey))

(facts "about game-accepts-as-guess?"
       (fact "determines if a given game will recognize a word as an attempt guess a word"
             (let [game {:word "strol"}]
               (trd/game-accepts-as-guess? game "sles") => falsey
               (trd/game-accepts-as-guess? game "kalja") => truthy
               (trd/game-accepts-as-guess? game "x-ray") => falsey)))

(facts "about create-game"
       (fact "creates a game for a given mode name and player"
             (trd/create-game "wordle" "ParaSait")
             => {:mode "wordle"
                 :mode-attrs {:word-length 5
                              :max-guesses 6}
                 :player "ParaSait"
                 :word "strol"
                 :guesses []
                 :max-guesses 6}
             (provided
              (trd/load-mode-data!)
              => {"wordle" {:word-length 5
                            :max-guesses 6}}
              :times 1
              (trd/pick-word 5)
              => "strol"
              :times 1))
       
       (fact "throws an exception if the mode doesn't exist"
             (trd/create-game "rurdle" "ParaSait")
             => (throws clojure.lang.ExceptionInfo)
             (provided
              (trd/load-mode-data!)
              => {"wordle" {:word-length 5
                            :max-guesses 6}}
              :times 1
              (trd/pick-word anything)
              => nil
              :times 0)))

(facts "about decide-score"
       (fact "the game has not yet been played, the score is not yet decided"
             (trd/decide-score {:word "sword"
                                :guesses []
                                :max-guesses 6})
             => nil?)
       
       (fact "if the word is not yet guessed, the score is not yet decided"
             (trd/decide-score {:word "sword"
                                :guesses ["strol"
                                          "aoben"
                                          "group"
                                          "chair"]
                                :max-guesses 6})
             => nil?)
       
       (fact "if the word is guessed, the score is the number of guesses"
             (trd/decide-score {:word "sword"
                                :guesses ["strol"
                                          "aoben"
                                          "group"
                                          "sword"]
                                :max-guesses 6})
             => 4)
       
       (fact "if the word is guessed is on the last turn, the score is the number of max guesses"
             (trd/decide-score {:word "sword"
                                :guesses ["strol"
                                          "aoben"
                                          "group"
                                          "chair"
                                          "kalja"
                                          "sword"]
                                :max-guesses 6})
             => 6)
       
       (fact "if we've reached the max number of guesses without guessing the score, the score is determined to be 0"
             (trd/decide-score {:word "sword"
                                :guesses ["strol"
                                          "aoben"
                                          "group"
                                          "chair"
                                          "kalja"
                                          "kilju"]
                                :max-guesses 6})
             => 0))

(facts "about make-letter-counts"
       (fact "makes a map of individual char counts in a string"
             (trd/make-letter-counts "abcaba")
             => {\a 3
                 \b 2
                 \c 1}))

(facts "about make-breakdown"
       (fact "makes a breakdown of a guess, which says which letters are not in the word, which letters are somewhere in the word, and which letters are in the correct place"
             (trd/make-breakdown "sword" "strol")
             => [[0 \s trd/status-correct]
                 [1 \t trd/status-absent]
                 [2 \r trd/status-present]
                 [3 \o trd/status-present]
                 [4 \l trd/status-absent]])

       (fact "a breakdown of a guess doesn't indicate the presence of a letter more than once if it's not in the word more than once (the one that's indicated is the one that comes first)"
             (trd/make-breakdown "abcde" "xyzaa")
             => [[0 \x trd/status-absent]
                 [1 \y trd/status-absent]
                 [2 \z trd/status-absent]
                 [3 \a trd/status-present]
                 [4 \a trd/status-absent]])
       
       (fact "if a letter from a guess is in the right place, then unless it's also in another place, only the letter that's in the right place is marked as not absent"
             (trd/make-breakdown "major" "razor")
             => [[0 \r trd/status-absent]
                 [1 \a trd/status-correct]
                 [2 \z trd/status-absent]
                 [3 \o trd/status-correct]
                 [4 \r trd/status-correct]])

       (fact "if a letter is in the word twice, with one of them being in the right place and the other not, both are marked appropriately"
             (trd/make-breakdown "abcda" "xyzaa")
             => [[0 \x trd/status-absent]
                 [1 \y trd/status-absent]
                 [2 \z trd/status-absent]
                 [3 \a trd/status-present]
                 [4 \a trd/status-correct]]))

(facts "about gather-knowledge-from-game"
       (fact "gathers the body of knowledge obtained from breakdowns of all guesses in a game"
             (trd/gather-knowledge-from-game {:word "sword"
                                              :guesses ["strol"
                                                        "aoben"
                                                        "group"
                                                        "chair"]})
             => [[0 \s trd/status-correct]
                 [1 \t trd/status-absent]
                 [2 \r trd/status-present]
                 [3 \o trd/status-present]
                 [4 \l trd/status-absent]
                 [0 \a trd/status-absent]
                 [1 \o trd/status-present]
                 [2 \b trd/status-absent]
                 [3 \e trd/status-absent]
                 [4 \n trd/status-absent]
                 [0 \g trd/status-absent]
                 [1 \r trd/status-present]
                 [2 \o trd/status-correct]
                 [3 \u trd/status-absent]
                 [4 \p trd/status-absent]
                 [0 \c trd/status-absent]
                 [1 \h trd/status-absent]
                 [2 \a trd/status-absent]
                 [3 \i trd/status-absent]
                 [4 \r trd/status-present]])
       
       (fact "doesn't repeat old information"
             (trd/gather-knowledge-from-game {:word "sword"
                                              :guesses ["strol"
                                                        "strop"]})
             => [[0 \s trd/status-correct]
                 [1 \t trd/status-absent]
                 [2 \r trd/status-present]
                 [3 \o trd/status-present]
                 [4 \l trd/status-absent]
                 [4 \p trd/status-absent]]))

(facts "about make-letter-report-from-knowledge"
       (fact "aggregates a breakdown into a map reporting the ultimate status of each letter in it"
             (trd/make-letter-report-from-knowledge
              [[0  \a trd/status-absent]
               [1  \b trd/status-present]
               [2  \c trd/status-correct]
               [3  \d trd/status-absent]
               [4  \d trd/status-present]
               [5  \e trd/status-absent]
               [6  \e trd/status-correct]
               [7  \f trd/status-present]
               [8  \f trd/status-correct]
               [9  \g trd/status-absent]
               [10 \g trd/status-present]
               [11 \g trd/status-correct]
               [12 \h trd/status-correct]
               [13 \h trd/status-present]
               [14 \h trd/status-absent]])
             => {\a trd/status-absent
                 \b trd/status-present
                 \c trd/status-correct
                 \d trd/status-present
                 \e trd/status-correct
                 \f trd/status-correct
                 \g trd/status-correct
                 \h trd/status-correct}))

(facts "about make-letter-report-for-game"
       (fact "makes a an aggregation of the breakdowns of all guesses in a game"
             (trd/make-letter-report-for-game {:word "sword"
                                               :guesses ["strol"
                                                         "aoben"
                                                         "group"
                                                         "chair"]})
             => {\a trd/status-absent
                 \b trd/status-absent
                 \c trd/status-absent
                 \e trd/status-absent
                 \g trd/status-absent
                 \h trd/status-absent
                 \i trd/status-absent
                 \l trd/status-absent
                 \n trd/status-absent
                 \o trd/status-correct
                 \p trd/status-absent
                 \r trd/status-present
                 \s trd/status-correct
                 \t trd/status-absent
                 \u trd/status-absent}))

(facts "about render-game"
       (fact "renders game state as a discord message"
             (trd/render-game {:word "sword"
                               :guesses ["strol"
                                         "aoben"
                                         "group"
                                         "chair"]
                               :max-guesses 6})
             => (str ">>> **` S `** ~~`[T]`~~ **`[R]`** **`[O]`** ~~`[L]`~~\n"
                         "~~`[A]`~~ **`[O]`** ~~`[B]`~~ ~~`[E]`~~ ~~`[N]`~~\n"
                         "~~`[G]`~~ **`[R]`** **` O `** ~~`[U]`~~ ~~`[P]`~~\n"
                         "~~`[C]`~~ ~~`[H]`~~ ~~`[A]`~~ ~~`[I]`~~ **`[R]`**\n"
                         "`[-]` `[-]` `[-]` `[-]` `[-]`\n"
                         "`[-]` `[-]` `[-]` `[-]` `[-]`\n"
                         "\n"
                         "D F J K M **O** Q __R__ **S** V W X Y Z"))
       
       (fact "can render an unplayed game"
             (trd/render-game {:word "sword"
                               :guesses []
                               :max-guesses 6})
             => (str ">>> `[-]` `[-]` `[-]` `[-]` `[-]`\n"
                         "`[-]` `[-]` `[-]` `[-]` `[-]`\n"
                         "`[-]` `[-]` `[-]` `[-]` `[-]`\n"
                         "`[-]` `[-]` `[-]` `[-]` `[-]`\n"
                         "`[-]` `[-]` `[-]` `[-]` `[-]`\n"
                         "`[-]` `[-]` `[-]` `[-]` `[-]`\n"
                         "\n"
                         "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z"))
       
       (fact "letters aren't indicated as being in a word more than once if they aren't"
             (trd/render-game {:word "major"
                               :guesses ["bloat"
                                         "croak"
                                         "aggro"
                                         "hoard"
                                         "razor"
                                         "manor"]
                               :max-guesses 6})
             => (str ">>> ~~`[B]`~~ ~~`[L]`~~ **`[O]`** **`[A]`** ~~`[T]`~~\n"
                         "~~`[C]`~~ **`[R]`** **`[O]`** **`[A]`** ~~`[K]`~~\n"
                         "**`[A]`** ~~`[G]`~~ ~~`[G]`~~ **`[R]`** **`[O]`**\n"
                         "~~`[H]`~~ **`[O]`** **`[A]`** **`[R]`** ~~`[D]`~~\n"
                         "~~`[R]`~~ **` A `** ~~`[Z]`~~ **` O `** **` R `**\n"
                         "**` M `** **` A `** ~~`[N]`~~ **` O `** **` R `**\n"
                         "\n"
                         "**A** E F I J **M** **O** P Q **R** S U V W X Y")))

(facts "about render-user-score-section"
       (fact "formats the score part of a stats object as a discord message and underscores a specific score"
             (trd/render-user-score-section
              {"scores" [103 31 32 33 34 35 36]
               "streak" 5
               "best-streak" 15}
              4)
             => (str "dead - **103**\n"
                     "1 guess - **31**\n"
                     "2 guesses - **32**\n"
                     "3 guesses - **33**\n"
                     "__4 guesses - **34**__\n"
                     "5 guesses - **35**\n"
                     "6 guesses - **36**")))

(facts "about render-user-streak-section"
       (fact "formats streak info for a given player in a score data object"
             (trd/render-user-streak-section
              {"scores" [103 31 32 33 34 35 36]
               "streak" 5
               "best-streak" 15}
              [20 ["ParaSait" "nyasudah"]] --username-f--)
             => (str "Current streak - **5**\n"
                     "Personal best streak - **15**\n"
                     "Overall best streak - **20** (Para's name, nya's name)")
             (provided
              (--username-f-- "ParaSait") => "Para's name"
              (--username-f-- "nyasudah") => "nya's name")))

(facts "about render-user-scores"
       (fact "loads and renders score data for a user"
             (trd/render-user-scores "nerdle" "ParaSait" --username-f--)
             => (str "Scores for Para's name:\n"
                     "dead - **103**\n"
                     "1 guess - **31**\n"
                     "2 guesses - **32**\n"
                     "3 guesses - **33**\n"
                     "4 guesses - **34**\n"
                     "5 guesses - **35**\n"
                     "6 guesses - **36**")
             (provided
              (ts/load-score-data!)
              => ..score-data..
              (ts/find-stats-for-user ..score-data.. "nerdle" "ParaSait")
              => {"scores" [103 31 32 33 34 35 36]
                  "streak" 5
                  "best-streak" 15}
              (--username-f-- "ParaSait")
              => "Para's name")))

(facts "about render-streak-overview"
       (fact "loads and renders the streak overview"
             (trd/render-streak-overview "nerdle" --username-f--)
             => (str "Streak overview:\n"
                     "Para's name - Current: 1, Best: 2.\n"
                     "nya's name - Current: **3**, Best: **4**.")
             (provided
              (ts/load-score-data!)
              => ..score-data..
              (ts/query-streak-overview-info ..score-data.. "nerdle")
              => {"ParaSait" {:streak      1
                              :best-streak 2}
                  "nyasudah" {:streak      3
                              :best-streak 4}}
              (--username-f-- "ParaSait") => "Para's name"
              (--username-f-- "nyasudah") => "nya's name")))

(facts "about render-modes-overview"
       (fact "loads and renders an overview of all turdle modes sorted by popularity/alphabet"
             (trd/render-modes-overview)
             => (str "Available game modes:\n"
                     "**wordle** (played 2 times) - a super based game\n"
                     "**kurdle** (played 1 time) - the basedest game of all\n"
                     "**nerdle** (played 1 time) - an even more based game\n"
                     "**nullle** (not played yet) - a game that has no record of being played")
             (provided
              (trd/load-mode-data!)
              => {"wordle" {:description "a super based game"}
                  "nerdle" {:description "an even more based game"}
                  "kurdle" {:description "the basedest game of all"}
                  "nullle" {:description "a game that has no record of being played"}}
              (ts/load-score-data!)
              => ..score-data..
              (ts/query-times-played-per-mode ..score-data..)
              => {"wordle" 2
                  "nerdle" 1
                  "kurdle" 1})))

(facts "about render-user-stats"
       (fact "formats a score string for the given user"
             (trd/render-user-stats
              {"wordle" {"ParaSait" {"scores" [103 31 32 33 34 35 36]
                                     "streak" 5
                                     "best-streak" 15}
                         "nyasudah" {"scores" [103 31 32 33 34 35 36]
                                     "streak" 4
                                     "best-streak" 14}
                         "jinx"     {"scores" [103 31 32 33 34 35 36]
                                     "streak" 5
                                     "best-streak" 15}}}
              "wordle" "nyasudah" 4 --username-f--)
             => (str "**__Scores__**\n"
                     "dead - **103**\n"
                     "1 guess - **31**\n"
                     "2 guesses - **32**\n"
                     "3 guesses - **33**\n"
                     "__4 guesses - **34**__\n"
                     "5 guesses - **35**\n"
                     "6 guesses - **36**\n"
                     "\n"
                     "**__Streaks__**\n"
                     "Current streak - **4**\n"
                     "Personal best streak - **14**\n"
                     "Overall best streak - **15** (Para's name, jinx' name)")
             (provided
              (--username-f-- "ParaSait") => "Para's name" :times 1
              (--username-f-- "jinx") => "jinx' name" :times 1))

       (fact "underlines the 'dead' score if you lost"
             (trd/render-user-stats
              {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                     "streak" 1
                                     "best-streak" 11}
                         "nyasudah" {"scores" [102 21 22 23 24 25 26]
                                     "streak" 2
                                     "best-streak" 12}}}
              "wordle" "ParaSait" 0 --username-f--)
             => (str "**__Scores__**\n"
                     "__dead - **101**__\n"
                     "1 guess - **11**\n"
                     "2 guesses - **12**\n"
                     "3 guesses - **13**\n"
                     "4 guesses - **14**\n"
                     "5 guesses - **15**\n"
                     "6 guesses - **16**\n"
                     "\n"
                     "**__Streaks__**\n"
                     "Current streak - **1**\n"
                     "Personal best streak - **11**\n"
                     "Overall best streak - **12** (nya's name)")
             (provided
              (--username-f-- "nyasudah") => "nya's name" :times 1))

       (fact "says you're amazing if you get it in 3 turns"
             (trd/render-user-stats
              {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                     "streak" 1
                                     "best-streak" 11}
                         "nyasudah" {"scores" [102 21 22 23 24 25 26]
                                     "streak" 2
                                     "best-streak" 12}}}
              "wordle" "ParaSait" 3 --username-f--)
             => (str "**__Scores__**\n"
                     "dead - **101**\n"
                     "1 guess - **11**\n"
                     "2 guesses - **12**\n"
                     "__3 guesses - **13**__\n"
                     "4 guesses - **14**\n"
                     "5 guesses - **15**\n"
                     "6 guesses - **16**\n"
                     (str e/dd " **_Amazing_** " e/dd "\n")
                     "\n"
                     "**__Streaks__**\n"
                     "Current streak - **1**\n"
                     "Personal best streak - **11**\n"
                     "Overall best streak - **12** (nya's name)")
             (provided
              (--username-f-- "nyasudah") => "nya's name" :times 1))

       (fact "says you're based if you get it in 2 turns"
             (trd/render-user-stats
              {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                     "streak" 1
                                     "best-streak" 11}
                         "nyasudah" {"scores" [102 21 22 23 24 25 26]
                                     "streak" 2
                                     "best-streak" 12}}}
              "wordle" "ParaSait" 2 --username-f--)
             => (str "**__Scores__**\n"
                     "dead - **101**\n"
                     "1 guess - **11**\n"
                     "__2 guesses - **12**__\n"
                     "3 guesses - **13**\n"
                     "4 guesses - **14**\n"
                     "5 guesses - **15**\n"
                     "6 guesses - **16**\n"
                     (str e/menri " **_Based_** " e/menri "\n")
                     "\n"
                     "**__Streaks__**\n"
                     "Current streak - **1**\n"
                     "Personal best streak - **11**\n"
                     "Overall best streak - **12** (nya's name)")
             (provided
              (--username-f-- "nyasudah") => "nya's name" :times 1))
       
       (fact "says you're freakin' dovahkiin if you get it in 1 turn"
             (trd/render-user-stats
              {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                     "streak" 1
                                     "best-streak" 11}
                         "nyasudah" {"scores" [102 21 22 23 24 25 26]
                                     "streak" 2
                                     "best-streak" 12}}}
              "wordle" "ParaSait" 1 --username-f--)
             => (str "**__Scores__**\n"
                     "dead - **101**\n"
                     "__1 guess - **11**__\n"
                     "2 guesses - **12**\n"
                     "3 guesses - **13**\n"
                     "4 guesses - **14**\n"
                     "5 guesses - **15**\n"
                     "6 guesses - **16**\n"
                     (str e/dovahkiin " **_DOVAHKIIN_** " e/dovahkiin "\n")
                     "\n"
                     "**__Streaks__**\n"
                     "Current streak - **1**\n"
                     "Personal best streak - **11**\n"
                     "Overall best streak - **12** (nya's name)")
             (provided
              (--username-f-- "nyasudah") => "nya's name" :times 1))

       (fact "shows a notice if you get a new personal best"
             (trd/render-user-stats
              {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                     "streak" 11
                                     "best-streak" 11}
                         "nyasudah" {"scores" [102 21 22 23 24 25 26]
                                     "streak" 2
                                     "best-streak" 12}}}
              "wordle" "ParaSait" 4 --username-f--)
             => (str "**__Scores__**\n"
                     "dead - **101**\n"
                     "1 guess - **11**\n"
                     "2 guesses - **12**\n"
                     "3 guesses - **13**\n"
                     "__4 guesses - **14**__\n"
                     "5 guesses - **15**\n"
                     "6 guesses - **16**\n"
                     "\n"
                     "**__Streaks__**\n"
                     "Current streak - **11**\n"
                     "Personal best streak - **11**\n"
                     "Overall best streak - **12** (nya's name)\n"
                     (str e/clap " **_Personal best streak!_** " e/clap))
             (provided
              (--username-f-- "nyasudah") => "nya's name" :times 1))

       (fact "shows a notice if you get a new overall best"
             (trd/render-user-stats
              {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                     "streak" 13
                                     "best-streak" 13}
                         "nyasudah" {"scores" [102 21 22 23 24 25 26]
                                     "streak" 2
                                     "best-streak" 12}}}
              "wordle" "ParaSait" 4 --username-f--)
             => (str "**__Scores__**\n"
                     "dead - **101**\n"
                     "1 guess - **11**\n"
                     "2 guesses - **12**\n"
                     "3 guesses - **13**\n"
                     "__4 guesses - **14**__\n"
                     "5 guesses - **15**\n"
                     "6 guesses - **16**\n"
                     "\n"
                     "**__Streaks__**\n"
                     "Current streak - **13**\n"
                     "Personal best streak - **13**\n"
                     "Overall best streak - **13** (Para's name)\n"
                     (str e/heyhey " **_Overall best streak!_** " e/heyhey))
             (provided
              (--username-f-- "ParaSait") => "Para's name" :times 1))
       
       (fact "can list multiple users as holding the overall best streak"
             (trd/render-user-stats
              {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                     "streak" 5
                                     "best-streak" 15}
                         "nyasudah" {"scores" [103 31 32 33 34 35 36]
                                     "streak" 4
                                     "best-streak" 14}
                         "jinx"     {"scores" [101 11 12 13 14 15 16]
                                     "streak" 5
                                     "best-streak" 15}}}
              "wordle" "nyasudah" 4 --username-f--)
             => (str "**__Scores__**\n"
                     "dead - **103**\n"
                     "1 guess - **31**\n"
                     "2 guesses - **32**\n"
                     "3 guesses - **33**\n"
                     "__4 guesses - **34**__\n"
                     "5 guesses - **35**\n"
                     "6 guesses - **36**\n"
                     "\n"
                     "**__Streaks__**\n"
                     "Current streak - **4**\n"
                     "Personal best streak - **14**\n"
                     "Overall best streak - **15** (Para's name, jinx' name)")
             (provided
              (--username-f-- "ParaSait") => "Para's name" :times 1
              (--username-f-- "jinx") => "jinx' name" :times 1)))