(ns kaljabot.bullshit-test
  (:require [midje.sweet :refer :all]
            [clojure.string :as str]
            [kaljabot.bullshit :as bs]))

(def test-bullshit
  (str/join "\n" ["-----------------"
                  "-ooo-ooo-ooo-ooo-"
                  "--o--o---o----o--"
                  "--o--oo--ooo--o--"
                  "--o--o-----o--o--"
                  "--o--ooo-ooo--o--"
                  "-----------------"]))

(def test-bullshit-number
  (str/join "\n" ["-------------"
                  "-oo--ooo-oo--"
                  "--o----o---o-"
                  "--o--ooo-oo--"
                  "--o--o-----o-"
                  "-ooo-ooo-oo--"
                  "-------------"]))

(facts "about generate-bullshit"
       (fact "it smells"
             (bs/generate-bullshit "Test" "o" "-") => test-bullshit)
       (fact "unknown characters are filtered"
             (bs/generate-bullshit "te$st" "o" "-") => test-bullshit)
       (fact "it retains digits"
             (bs/generate-bullshit "123" "o" "-") => test-bullshit-number))
