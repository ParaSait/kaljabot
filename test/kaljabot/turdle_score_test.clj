(ns kaljabot.turdle-score-test
  (:require [midje.sweet :refer :all]
            [kaljabot.turdle-score :as ts]
            [kaljabot.constants :as c]
            [kaljabot.emoji :as e]))

(facts "about tally-score!"
       (fact "increments a tally count of one of a particular player's ranks in the score file, and returns the new scores"
             (let [old-scores {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                                     "streak" 1
                                                     "best-streak" 11}}
                               "nerdle" {"ParaSait" {"scores" [102 21 22 23 24 25 26]
                                                     "streak" 2
                                                     "best-streak" 12}
                                         "nyasudah" {"scores" [103 31 32 33 34 35 36]
                                                     "streak" 3
                                                     "best-streak" 13}}
                               "quordle" {"nyasudah" {"scores" [104 41 42 43 44 45 46]
                                                      "streak" 4
                                                      "best-streak" 14}}}
                   new-scores {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                                     "streak" 1
                                                     "best-streak" 11}}
                               "nerdle" {"ParaSait" {"scores" [102 21 22 23 24 25 26]
                                                     "streak" 2
                                                     "best-streak" 12}
                                         "nyasudah" {"scores" [103 31 32 34 34 35 36]
                                                     "streak" 4
                                                     "best-streak" 13}}
                               "quordle" {"nyasudah" {"scores" [104 41 42 43 44 45 46]
                                                      "streak" 4
                                                      "best-streak" 14}}}]
               (ts/tally-score! "nerdle" {:max-guesses 6} "nyasudah" 3)
               => new-scores
               (provided
                (ts/load-score-data!)
                => old-scores :times 1
                (ts/save-score-data! new-scores)
                => nil :times 1)))

       (fact "updates the best streak if it's been broken"
             (let [old-scores {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                                     "streak" 3
                                                     "best-streak" 3}}}
                   new-scores {"wordle" {"ParaSait" {"scores" [101 11 12 14 14 15 16]
                                                     "streak" 4
                                                     "best-streak" 4}}}]
               (ts/tally-score! "wordle" {:max-guesses 6} "ParaSait" 3)
               => new-scores
               (provided
                (ts/load-score-data!)
                => old-scores :times 1
                (ts/save-score-data! new-scores)
                => nil :times 1)))

       (fact "resets the streak if we're incrementing the 'score 0' (= 'dead') counter"
             (let [old-scores {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                                     "streak" 3
                                                     "best-streak" 3}}}
                   new-scores {"wordle" {"ParaSait" {"scores" [102 11 12 13 14 15 16]
                                                     "streak" 0
                                                     "best-streak" 3}}}]
               (ts/tally-score! "wordle" {:max-guesses 6} "ParaSait" 0)
               => new-scores
               (provided
                (ts/load-score-data!)
                => old-scores :times 1
                (ts/save-score-data! new-scores)
                => nil :times 1)))
       
       (fact "creates a user when nonexistent for a mode"
             (let [old-scores {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                                     "streak" 3
                                                     "best-streak" 3}}}
                   new-scores {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                                     "streak" 3
                                                     "best-streak" 3}
                                         "nyasudah" {"scores" [0 0 0 1 0 0 0]
                                                     "streak" 1
                                                     "best-streak" 1}}}]
               (ts/tally-score! "wordle" {:max-guesses 6} "nyasudah" 3)
               => new-scores
               (provided
                (ts/load-score-data!)
                => old-scores :times 1
                (ts/save-score-data! new-scores)
                => nil :times 1)))
       
       (fact "creates a new mode when nonexistent"
             (let [old-scores {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                                     "streak" 3
                                                     "best-streak" 3}}}
                   new-scores {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                                     "streak" 3
                                                     "best-streak" 3}}
                               "nerdle" {"ParaSait" {"scores" [0 0 0 1 0 0 0]
                                                     "streak" 1
                                                     "best-streak" 1}}}]
               (ts/tally-score! "nerdle" {:max-guesses 6} "ParaSait" 3)
               => new-scores
               (provided
                (ts/load-score-data!)
                => old-scores :times 1
                (ts/save-score-data! new-scores)
                => nil :times 1)))
       
       (fact "creates an initial user with an initial rank when the scores themselves are nonexistent"
             (let [old-scores nil
                   new-scores {"wordle" {"ParaSait" {"scores" [0 0 0 1 0 0 0]
                                                     "streak" 1
                                                     "best-streak" 1}}}]
               (ts/tally-score! "wordle" {:max-guesses 6} "ParaSait" 3)
               => new-scores
               (provided
                (ts/load-score-data!)
                => old-scores :times 1
                (ts/save-score-data! new-scores)
                => nil :times 1))))

(facts "about query-streak-overview-info"
       (fact "queries streak information for all relevant players"
             (ts/query-streak-overview-info
              {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                     "streak" 1
                                     "best-streak" 11}
                         "nyasudah" {"scores" [103 31 32 33 34 35 36]
                                     "streak" 2
                                     "best-streak" 12}
                         "jinx"     {"scores" [103 31 32 33 34 35 36]
                                     "streak" 3
                                     "best-streak" 12}}
               "nerdle" {"ParaSait" {"scores" [102 21 22 23 24 25 26]
                                     "streak" 3
                                     "best-streak" 13}}}
              "wordle")
             => {"ParaSait" {:streak 1 :best-streak 11}
                 "nyasudah" {:streak 2 :best-streak 12}
                 "jinx"     {:streak 3 :best-streak 12}}))

(facts "about query-overall-best-streak-info"
       (fact "queries the best streak along with the best streaking players"
             (ts/query-overall-best-streak-info
              {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                     "streak" 1
                                     "best-streak" 11}
                         "nyasudah" {"scores" [103 31 32 33 34 35 36]
                                     "streak" 2
                                     "best-streak" 12}
                         "jinx"     {"scores" [103 31 32 33 34 35 36]
                                     "streak" 3
                                     "best-streak" 12}}
               "nerdle" {"ParaSait" {"scores" [102 21 22 23 24 25 26]
                                     "streak" 3
                                     "best-streak" 13}}}
              "wordle")
             => (just [12 (just ["nyasudah" "jinx"] :in-any-order)])))

(facts "about find-stats-for-user"
       (fact "finds stats for given user in given mode"
             (ts/find-stats-for-user
              {"wordle" {"ParaSait" {"scores" [101 11 12 13 14 15 16]
                                     "streak" 1
                                     "best-streak" 11}
                         "nyasudah" {"scores" [103 31 32 33 34 35 36]
                                     "streak" 2
                                     "best-streak" 12}
                         "jinx"     {"scores" [103 31 32 33 34 35 36]
                                     "streak" 3
                                     "best-streak" 12}}
               "nerdle" {"ParaSait" {"scores" [102 21 22 23 24 25 26]
                                     "streak" 3
                                     "best-streak" 13}}}
              "wordle" "ParaSait")
             => {"scores" [101 11 12 13 14 15 16]
                 "streak" 1
                 "best-streak" 11}))

(facts "about query-times-played-per-mode"
       (fact "queries the best streak along with the best streaking players"
             (ts/query-times-played-per-mode
              {"wordle" {"ParaSait" {"scores" [1 2 3 4 5 6 7]
                                     "streak" 1
                                     "best-streak" 11}
                         "nyasudah" {"scores" [2 3 4 5 6 7 8]
                                     "streak" 2
                                     "best-streak" 12}
                         "jinx"     {"scores" [3 4 5 6 7 8 9]
                                     "streak" 3
                                     "best-streak" 12}}
               "nerdle" {"ParaSait" {"scores" [2 4 6 8 10 12 14]
                                     "streak" 3
                                     "best-streak" 13}}})
             => {"wordle" 105
                 "nerdle" 56}))