(ns kaljabot.bot-test
  (:require [midje.sweet :refer :all]
            [kaljabot.bot :as b]
            [kaljabot.discord-api :as da]))

(facts "about mention-pattern"
       (re-matches b/mention-pattern "<@463840202050502676>") => truthy
       (re-matches b/mention-pattern "<@!463840202050502676>") => truthy
       (re-matches b/mention-pattern "<@notanumber>") => falsey
       (re-matches b/mention-pattern "<@>") => falsey
       (re-matches b/mention-pattern "<@!>") => falsey
       (re-matches b/mention-pattern "bullshit") => falsey)

(facts "about strip-mentions"
       (fact "removes mentions from a message"
             (b/strip-mentions "<@463840202050502676> foo bar <@463840202050502676> baz qux <@463840202050502676>") => "foo bar baz qux")
       (fact "removes nickname mentions from a message"
             (b/strip-mentions "<@!463840202050502676> foo bar <@!463840202050502676> baz qux <@!463840202050502676>") => "foo bar baz qux")
       (fact "does nothing if a message contains no mentions"
             (b/strip-mentions "foo bar baz qux") => "foo bar baz qux"))

(facts "about parse-turdle-args"
       (fact "no arguments means playing the game in default mode"
             (b/parse-turdle-args [])
             => {:action :play})

       (fact "any other single argument is interpreted as playing the game in a particular mode"
             (b/parse-turdle-args ["nerdle"])
             => {:action :play
                 :mode "nerdle"})

       (fact "any other single argument with more additional arguments after that is invalid"
             (b/parse-turdle-args ["nerdle" "nyasudah"])
             => (throws clojure.lang.ExceptionInfo))

       (fact "the 'play' argument means playing the game in default mode"
             (b/parse-turdle-args ["play"])
             => {:action :play})

       (fact "the 'play' argument together with a mode means playing the game in a particular mode"
             (b/parse-turdle-args ["play" "nerdle"])
             => {:action :play
                 :mode "nerdle"})

       (fact "the 'play' argument with more than one additional argument is invalid"
             (b/parse-turdle-args ["play" "nerdle" "nyasudah"])
             => (throws clojure.lang.ExceptionInfo))

       (fact "the 'scores' argument together with a mode and a player name means showing stats for that player in that mode"
             (b/parse-turdle-args ["scores" "nerdle" "ParaSait"])
             => {:action :scores
                 :mode "nerdle"
                 :player-name "ParaSait"})
       
       (fact "the 'scores' argument together with a mode means showing the scores of the implied player (that being the sender) in that mode"
             (b/parse-turdle-args ["scores" "nerdle"])
             => {:action :scores
                 :mode "nerdle"})

       (fact "the 'scores' argument without anything else means showing the default mode scores of the implied player (that being the sender)"
             (b/parse-turdle-args ["scores"])
             => {:action :scores})
       
       (fact "the 'scores' argument with more than one additional argument is invalid"
             (b/parse-turdle-args ["scores" "nerdle" "ParaSait" "nyasudah"])
             => (throws clojure.lang.ExceptionInfo))

       (fact "the 'streaks' argument without anything else means showing a report of all default mode streaks"
             (b/parse-turdle-args ["streaks"])
             => {:action :streaks})

       (fact "the 'streaks' argument without anything else means showing a report of all streaks in that mode"
             (b/parse-turdle-args ["streaks" "nerdle"])
             => {:action :streaks
                 :mode "nerdle"})
       
       (fact "the 'streaks' argument with more than one additional argument is invalid"
             (b/parse-turdle-args ["streaks" "nerdle" "ParaSait"])
             => (throws clojure.lang.ExceptionInfo))
       
       (fact "the 'modes' argument without anything else means showing a report of all turdle modes"
             (b/parse-turdle-args ["modes"])
             => {:action :modes})
       
       (fact "the 'modes' argument with any additional argument is invalid"
             (b/parse-turdle-args ["modes" "nerdle"])
             => (throws clojure.lang.ExceptionInfo)))

(facts "about turdle-slashargs->oldargs"
       (fact "play without mode argument"
             (b/parse-turdle-args (b/turdle-slashargs->oldargs {:play {}}))
             => {:action :play})
       
       (fact "play with mode argument"
             (b/parse-turdle-args (b/turdle-slashargs->oldargs {:play {:mode "nerdle"}}))
             => {:action :play
                 :mode "nerdle"})

       (fact "scores without argument"
             (b/parse-turdle-args (b/turdle-slashargs->oldargs {:scores {}}))
             => {:action :scores})

       (fact "scores with mode argument"
             (b/parse-turdle-args (b/turdle-slashargs->oldargs {:scores {:mode "nerdle"}}))
             => {:action :scores
                 :mode "nerdle"})
       
       (fact "scores with mode and player argument"
             (b/parse-turdle-args (b/turdle-slashargs->oldargs {:scores {:mode "nerdle" :player 123}}))
             => {:action :scores
                 :mode "nerdle"
                 :player-name "ParaSait"}
             (provided
              (da/get-user! 123) => "ParaSait"))

       (fact "streaks without argument"
             (b/parse-turdle-args (b/turdle-slashargs->oldargs {:streaks {}}))
             => {:action :streaks})

       (fact "streaks with mode argument"
             (b/parse-turdle-args (b/turdle-slashargs->oldargs {:streaks {:mode "nerdle"}}))
             => {:action :streaks
                 :mode "nerdle"})
       
       (fact "modes"
             (b/parse-turdle-args (b/turdle-slashargs->oldargs {:modes {}}))
             => {:action :modes}))

(facts "about parse-pref-args"
       (fact "no arguments is invalid"
             (b/parse-pref-args [])
             => (throws clojure.lang.ExceptionInfo))

       (fact "any other argument is invalid"
             (b/parse-pref-args ["bogus"])
             => (throws clojure.lang.ExceptionInfo))
       
       (fact "the 'set' argument without something else is invalid"
             (b/parse-pref-args ["set"])
             => (throws clojure.lang.ExceptionInfo))
       
       (fact "the 'set' argument with a key but nothing else is invalid"
             (b/parse-pref-args ["set" "key"])
             => (throws clojure.lang.ExceptionInfo))
       
       (fact "the 'set' argument with a key and a value means setting the value for that key"
             (b/parse-pref-args ["set" "key" "value"])
             => {:action :set
                 :key "key"
                 :value "value"})

       (fact "the 'set' argument with a key, a value but then something after it too is invalid"
             (b/parse-pref-args ["set" "key" "value" "bogus"])
             => (throws clojure.lang.ExceptionInfo))
       
       (fact "the 'get' argument without something else is invalid"
             (b/parse-pref-args ["get"])
             => (throws clojure.lang.ExceptionInfo))
       
       (fact "the 'get' argument with a key means getting the value for that key"
             (b/parse-pref-args ["get" "key"])
             => {:action :get
                 :key "key"})
       
       (fact "the 'get' argument with a key but something else after it too is invalid"
             (b/parse-pref-args ["get" "key" "bogus"])
             => (throws clojure.lang.ExceptionInfo)))

(facts "about pref-slashargs->oldargs"
       (fact "setting a value"
             (b/parse-pref-args (b/pref-slashargs->oldargs {:set {:key "foo" :value "bar"}}))
             => {:action :set
                 :key "foo"
                 :value "bar"})
       
       (fact "getting a value"
             (b/parse-pref-args (b/pref-slashargs->oldargs {:get {:key "foo"}}))
             => {:action :get
                 :key "foo"}))

(facts "about looks-like-number?"
       (fact "true if string consists of a number"
             (b/looks-like-number? "123") => truthy)
       
       (fact "false if string contains a number, but doesn't purely consist of it"
             (b/looks-like-number? "123 strol") => falsey)
       
       (fact "false if string isn't a number"
             (b/looks-like-number? "strol") => falsey)
       
       (fact "false if string is empty"
             (b/looks-like-number? "") => falsey)

       (fact "true if string contains both a number and the word 'tomorrow'"
             (b/looks-like-number? "tomorrow's number is 5") => truthy
             (b/looks-like-number? "5 tomorrow") => truthy)
       
       (fact "false if string contains the word 'tomorrow', but no number"
             (b/looks-like-number? "tomorrow I will be there") => falsey))