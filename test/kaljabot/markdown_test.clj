(ns kaljabot.markdown-test
  (:require [kaljabot.markdown :as md]
            [midje.sweet :refer :all]))

(facts "about markdown functions"
       (tabular
        (fact "the functions apply markdown to given strings (and other stringable values), but empty strings should just remain empty strings"
              (?f ?i) => ?o)
        ?f               ?i          ?o
        md/bold          "something" "**something**"
        md/bold          123         "**123**"
        md/bold          ""          ""
        md/italic        "something" "_something_"
        md/italic        123         "_123_"
        md/italic        ""          ""
        md/underline     "something" "__something__"
        md/underline     123         "__123__"
        md/underline     ""          ""
        md/strikethrough "something" "~~something~~"
        md/strikethrough 123         "~~123~~"
        md/strikethrough ""          ""
        md/monospace     "something" "`something`"
        md/monospace     123         "`123`"
        md/monospace     ""          ""
        md/codeblock     "something" "```something```"
        md/codeblock     123         "```123```"
        md/codeblock     ""          ""
        md/multiquote    "something" ">>> something"
        md/multiquote    123         ">>> 123"
        md/multiquote    ""          ""))